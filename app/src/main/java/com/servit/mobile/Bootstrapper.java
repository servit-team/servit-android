package com.servit.mobile;

import android.app.Application;

import com.backendless.Backendless;
import com.servit.mobile.data.BusinessDataStore;
import com.servit.mobile.data.BusinessDataStoreBackendless;
import com.servit.mobile.data.PaymentDataStore;
import com.servit.mobile.data.PaymentDataStoreStripe;
import com.servit.mobile.data.UserDataStore;
import com.servit.mobile.data.UserDataStoreBackendless;
import com.servit.mobile.data.entities.ServitUser;
import com.servit.mobile.data.entities.business;
import com.servit.mobile.data.entities.owner;
import com.servit.mobile.framework.annotations.ServiceLocator;

/**
 * Created by Brandon on 6/9/2016.
 */

public class Bootstrapper extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Backendless.initApp( this, BuildConfig.BACKENDDLESS_APP_ID, BuildConfig.BACKENDDLESS_ANDROID_ID, "v1" );
        Backendless.setUrl(BuildConfig.BACKENDDLESS_URL);
        Backendless.Data.mapTableToClass("User", ServitUser.class );
        Backendless.Data.mapTableToClass("owner", owner.class);
        Backendless.Data.mapTableToClass("business", business.class);


        ServiceLocator.getInstance().put(UserDataStore.class, new UserDataStoreBackendless());
        ServiceLocator.getInstance().put(BusinessDataStore.class, new BusinessDataStoreBackendless());
        ServiceLocator.getInstance().put(PaymentDataStore.class, new PaymentDataStoreStripe(getApplicationContext()));
    }
}
