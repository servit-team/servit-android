package com.servit.mobile;

import android.content.Context;

/**
 * Created by Brandon on 6/20/2016.
 */

public class BaseFragmentListener<T> extends BaseFragment {
    protected T listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (T)context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        listener = null;
    }
}
