package com.servit.mobile.data.entities;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;

/**
 * Created by Brandon on 6/12/2016.
 */

public class ServitUser extends BackendlessUser
{
    public String getEmail()
    {
        return super.getEmail();
    }

    public void setEmail( String email )
    {
        super.setEmail( email );
    }

    public String getPassword()
    {
        return super.getPassword();
    }

    public String getFacebookId()
    {
        return (String) super.getProperty( "facebookId" );
    }

    public void setFacebookId( String facebookId )
    {
        super.setProperty( "facebookId", facebookId );
    }

    public String getFirstname()
    {
        return (String) super.getProperty( "firstname" );
    }

    public void setFirstname( String firstname )
    {
        super.setProperty( "firstname", firstname );
    }

    public String getLastname()
    {
        return (String) super.getProperty( "lastname" );
    }

    public void setLastname( String lastname )
    {
        super.setProperty( "lastname", lastname );
    }

    public String getPhone()
    {
        return (String) super.getProperty( "phone" );
    }

    public void setPhone( String phone )
    {
        super.setProperty( "phone", phone );
    }

    public String getPoints_total()
    {
        return (String) super.getProperty( "points_total" );
    }

    public void setPoints_total( String points_total )
    {
        super.setProperty( "points_total", points_total );
    }

    public String getStripeId()
    {
        return (String) super.getProperty( "stripeId" );
    }

    public void setStripeId( String stripeId )
    {
        super.setProperty( "stripeId", stripeId );
    }

    public address getAddress()
    {
        return (address) super.getProperty( "address" );
    }

    public void setAddress( address address )
    {
        super.setProperty( "address", address );
    }

    public employee getEmployee()
    {
        return (employee) super.getProperty( "employee" );
    }

    public void setEmployee( employee employee )
    {
        super.setProperty( "employee", employee );
    }

    public owner getOwner()
    {
        return (owner) super.getProperty( "owner" );
    }

    public void setOwner( owner owner )
    {
        super.setProperty( "owner", owner );
    }

    public boolean getIsSignupCompleted()
    {
        return (boolean) super.getProperty( "isSignupCompleted" );
    }

    public void setIsSignupCompleted( boolean owner )
    {
        super.setProperty( "isSignupCompleted", owner );
    }

    public ServitUser save()
    {
        return Backendless.Data.of( ServitUser.class ).save( this );
    }
    public static ServitUser fromBackendlessUser(BackendlessUser backendlessUser){
        ServitUser servitUser = new ServitUser();
        servitUser.setProperties(backendlessUser.getProperties());
        return servitUser;
    }
}
