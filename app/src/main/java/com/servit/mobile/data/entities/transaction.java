package com.servit.mobile.data.entities;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

public class transaction
{
  private String stripe_trans_id;
  private String hst;
  private String cost;
  private java.util.Date created;
  private String total;
  private String objectId;
  private java.util.Date updated;
  private String quantity;
  private String ownerId;
  private String stripe_tran_id;
  private String gst;
  private product_service product_service;
  public String getStripe_trans_id()
  {
    return stripe_trans_id;
  }

  public void setStripe_trans_id( String stripe_trans_id )
  {
    this.stripe_trans_id = stripe_trans_id;
  }

  public String getHst()
  {
    return hst;
  }

  public void setHst( String hst )
  {
    this.hst = hst;
  }

  public String getCost()
  {
    return cost;
  }

  public void setCost( String cost )
  {
    this.cost = cost;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public String getTotal()
  {
    return total;
  }

  public void setTotal( String total )
  {
    this.total = total;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public String getQuantity()
  {
    return quantity;
  }

  public void setQuantity( String quantity )
  {
    this.quantity = quantity;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public String getStripe_tran_id()
  {
    return stripe_tran_id;
  }

  public void setStripe_tran_id( String stripe_tran_id )
  {
    this.stripe_tran_id = stripe_tran_id;
  }

  public String getGst()
  {
    return gst;
  }

  public void setGst( String gst )
  {
    this.gst = gst;
  }

  public product_service getProduct_service()
  {
    return product_service;
  }

  public void setProduct_service( product_service product_service )
  {
    this.product_service = product_service;
  }

                                                    
  public transaction save()
  {
    return Backendless.Data.of( transaction.class ).save( this );
  }

  public Future<transaction> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<transaction> future = new Future<transaction>();
      Backendless.Data.of( transaction.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<transaction> callback )
  {
    Backendless.Data.of( transaction.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( transaction.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( transaction.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( transaction.class ).remove( this, callback );
  }

  public static transaction findById( String id )
  {
    return Backendless.Data.of( transaction.class ).findById( id );
  }

  public static Future<transaction> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<transaction> future = new Future<transaction>();
      Backendless.Data.of( transaction.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<transaction> callback )
  {
    Backendless.Data.of( transaction.class ).findById( id, callback );
  }

  public static transaction findFirst()
  {
    return Backendless.Data.of( transaction.class ).findFirst();
  }

  public static Future<transaction> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<transaction> future = new Future<transaction>();
      Backendless.Data.of( transaction.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<transaction> callback )
  {
    Backendless.Data.of( transaction.class ).findFirst( callback );
  }

  public static transaction findLast()
  {
    return Backendless.Data.of( transaction.class ).findLast();
  }

  public static Future<transaction> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<transaction> future = new Future<transaction>();
      Backendless.Data.of( transaction.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<transaction> callback )
  {
    Backendless.Data.of( transaction.class ).findLast( callback );
  }

  public static BackendlessCollection<transaction> find(BackendlessDataQuery query )
  {
    return Backendless.Data.of( transaction.class ).find( query );
  }

  public static Future<BackendlessCollection<transaction>> findAsync(BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<transaction>> future = new Future<BackendlessCollection<transaction>>();
      Backendless.Data.of( transaction.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync(BackendlessDataQuery query, AsyncCallback<BackendlessCollection<transaction>> callback )
  {
    Backendless.Data.of( transaction.class ).find( query, callback );
  }
}