package com.servit.mobile.data.entities;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

import java.io.Serializable;

public class business implements Serializable
{
  private String phone;
  private java.util.Date updated;
  private java.util.Date created;
  private String name;
  private String logo;
  private String objectId;
  private String ownerId;
  private address address;
  public String getPhone()
  {
    return phone;
  }

  public void setPhone( String phone )
  {
    this.phone = phone;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public String getName()
  {
    return name;
  }

  public void setName( String name )
  {
    this.name = name;
  }

  public String getLogo()
  {
    return logo;
  }

  public void setLogo( String logo )
  {
    this.logo = logo;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public address getAddress()
  {
    return address;
  }

  public void setAddress( address address )
  {
    this.address = address;
  }

                                                    
  public business save()
  {
    return Backendless.Data.of( business.class ).save( this );
  }

  public Future<business> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<business> future = new Future<business>();
      Backendless.Data.of( business.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<business> callback )
  {
    Backendless.Data.of( business.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( business.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( business.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( business.class ).remove( this, callback );
  }

  public static business findById( String id )
  {
    return Backendless.Data.of( business.class ).findById( id );
  }

  public static Future<business> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<business> future = new Future<business>();
      Backendless.Data.of( business.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<business> callback )
  {
    Backendless.Data.of( business.class ).findById( id, callback );
  }

  public static business findFirst()
  {
    return Backendless.Data.of( business.class ).findFirst();
  }

  public static Future<business> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<business> future = new Future<business>();
      Backendless.Data.of( business.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<business> callback )
  {
    Backendless.Data.of( business.class ).findFirst( callback );
  }

  public static business findLast()
  {
    return Backendless.Data.of( business.class ).findLast();
  }

  public static Future<business> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<business> future = new Future<business>();
      Backendless.Data.of( business.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<business> callback )
  {
    Backendless.Data.of( business.class ).findLast( callback );
  }

  public static BackendlessCollection<business> find(BackendlessDataQuery query )
  {
    return Backendless.Data.of( business.class ).find( query );
  }

  public static Future<BackendlessCollection<business>> findAsync(BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<business>> future = new Future<BackendlessCollection<business>>();
      Backendless.Data.of( business.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync(BackendlessDataQuery query, AsyncCallback<BackendlessCollection<business>> callback )
  {
    Backendless.Data.of( business.class ).find( query, callback );
  }
}