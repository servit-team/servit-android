package com.servit.mobile.data;

/**
 *  9/10/2015.
 */
public class ServiceResult<T> {
    boolean hasError;
    ErrorCode errorCode;
    private String reason;
    T result;

    public ServiceResult(){

    }

    public ServiceResult(ErrorCode errorCode){

        this.errorCode = errorCode;
        hasError = true;
    }

    public void success(T data){
        result = data;
    }

    public void fail(ErrorCode errorCode, T data){
        this.errorCode = errorCode;
        this.result = data;
        hasError = true;
    }

    public boolean getHasError(){
        return hasError;
    }

    public ErrorCode getErrorCode(){
        return errorCode;
    }

    public String getErrorReason(){
        return reason;
    }

    public T getResult(){
        return result;
    }
}
