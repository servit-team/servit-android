package com.servit.mobile.data;

import android.os.AsyncTask;

import com.backendless.persistence.BackendlessDataQuery;
import com.servit.mobile.data.entities.business;
import com.servit.mobile.data.entities.product_service;

import java.util.List;

/**
 * Created by Brandon on 6/12/2016.
 */

public class BusinessDataStoreBackendless implements BusinessDataStore {
    @Override
    public void getBusinessById(final IServiceHandler<business> handler) {
        if (handler == null){
            throw new IllegalArgumentException("handler must not be null");
        }

        new AsyncTask(){
            @Override
            protected Object doInBackground(Object[] params) {
                return null;// business.findById(UserSessionManager.getInstance().getCurrentUser().getOwner().getBusiness());
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                ServiceResult result = new ServiceResult();
                result.success(o);

                handler.onComplete(result);
            }
        }.execute();
    }

    @Override
    public void loadProducts(final IServiceHandler<List<product_service>> handler) {

        if (handler == null){
            throw new IllegalArgumentException("handler must not be null");
        }

        new AsyncTask<Object, Integer, List<product_service>>(){
            @Override
            protected List<product_service> doInBackground(Object[] params) {
                BackendlessDataQuery dataQuery = new BackendlessDataQuery();
                return product_service.find(dataQuery).getData();
            }

            @Override
            protected void onPostExecute(List<product_service> o) {
                super.onPostExecute(o);

                ServiceResult result = new ServiceResult();
                result.success(o);

                handler.onComplete(result);
            }
        }.execute();

    }

    @Override
    public void addProduct(final product_service newProduct, final IServiceHandler<Void> handler) {
        if (handler == null){
            throw new IllegalArgumentException("handler must not be null");
        }

        new AsyncTask<Object, Integer, Void>(){
            @Override
            protected Void doInBackground(Object[] params) {
                newProduct.save();
                return null;
            }

            @Override
            protected void onPostExecute(Void o) {
                super.onPostExecute(o);

                ServiceResult result = new ServiceResult();
                result.success(null);

                handler.onComplete(result);
            }
        }.execute();
    }

    @Override
    public void saveBusiness(final business business, final IServiceHandler<Void> handler) {
        if (handler == null){
            throw new IllegalArgumentException("handler must not be null");
        }

        new AsyncTask<Object, Integer, Void>(){
            @Override
            protected Void doInBackground(Object[] params) {
                business.save();
                return null;
            }

            @Override
            protected void onPostExecute(Void o) {
                super.onPostExecute(o);

                ServiceResult result = new ServiceResult();
                result.success(null);

                handler.onComplete(result);
            }
        }.execute();
    }
}
