package com.servit.mobile.data;

import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

/**
 * Created by Brandon on 6/20/2016.
 */

public interface PaymentDataStore {
    void getToken(Card card, IServiceHandler<Token> hander);
    void sendToken(String email, String token, final IServiceHandler<Void> handler);
}
