package com.servit.mobile.data.entities;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

public class category
{
  private String name;
  private java.util.Date updated;
  private String ownerId;
  private java.util.Date created;
  private String objectId;
  public String getName()
  {
    return name;
  }

  public void setName( String name )
  {
    this.name = name;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public String getObjectId()
  {
    return objectId;
  }

                                                    
  public category save()
  {
    return Backendless.Data.of( category.class ).save( this );
  }

  public Future<category> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<category> future = new Future<category>();
      Backendless.Data.of( category.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<category> callback )
  {
    Backendless.Data.of( category.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( category.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( category.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( category.class ).remove( this, callback );
  }

  public static category findById( String id )
  {
    return Backendless.Data.of( category.class ).findById( id );
  }

  public static Future<category> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<category> future = new Future<category>();
      Backendless.Data.of( category.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<category> callback )
  {
    Backendless.Data.of( category.class ).findById( id, callback );
  }

  public static category findFirst()
  {
    return Backendless.Data.of( category.class ).findFirst();
  }

  public static Future<category> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<category> future = new Future<category>();
      Backendless.Data.of( category.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<category> callback )
  {
    Backendless.Data.of( category.class ).findFirst( callback );
  }

  public static category findLast()
  {
    return Backendless.Data.of( category.class ).findLast();
  }

  public static Future<category> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<category> future = new Future<category>();
      Backendless.Data.of( category.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<category> callback )
  {
    Backendless.Data.of( category.class ).findLast( callback );
  }

  public static BackendlessCollection<category> find(BackendlessDataQuery query )
  {
    return Backendless.Data.of( category.class ).find( query );
  }

  public static Future<BackendlessCollection<category>> findAsync(BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<category>> future = new Future<BackendlessCollection<category>>();
      Backendless.Data.of( category.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync(BackendlessDataQuery query, AsyncCallback<BackendlessCollection<category>> callback )
  {
    Backendless.Data.of( category.class ).find( query, callback );
  }
}