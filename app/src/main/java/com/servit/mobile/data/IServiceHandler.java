package com.servit.mobile.data;

public interface IServiceHandler<T> {
    void onComplete(ServiceResult<T> result);
}
