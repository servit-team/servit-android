package com.servit.mobile.data.entities;

/**
 * Created by Brandon on 6/10/2016.
 */
public class Reward {
    String name;

    public Reward(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }
}