package com.servit.mobile.data;

import android.os.AsyncTask;

import com.backendless.persistence.BackendlessDataQuery;
import com.servit.mobile.data.entities.business;
import com.servit.mobile.data.entities.customer;

import java.util.List;

/**
 * Created by Brandon on 6/23/2016.
 */

public class CustomerDataStoreBackendless implements CustomerDataStore {
    @Override
    public void getCustomersForBusiness(final business business, final IServiceHandler<List<customer>> handler) {
        if (handler == null){
            throw new IllegalArgumentException("handler must not be null");
        }

        new AsyncTask<Object, Integer, List<customer>>(){
            @Override
            protected List<customer> doInBackground(Object[] params) {

                BackendlessDataQuery dataQuery = new BackendlessDataQuery();
                dataQuery.setWhereClause("business.objectId='" + business.getObjectId() + "'");
                return customer.find(dataQuery).getData();
            }

            @Override
            protected void onPostExecute(List<customer> o) {
                super.onPostExecute(o);

                ServiceResult result = new ServiceResult();
                result.success(o);

                handler.onComplete(result);
            }
        }.execute();
    }

    @Override
    public void addCustomerForBusiness(final business business, final IServiceHandler<Void> handler) {
        if (handler == null){
            throw new IllegalArgumentException("handler must not be null");
        }

        new AsyncTask<Object, Integer, List<customer>>(){
            @Override
            protected List<customer> doInBackground(Object[] params) {

                BackendlessDataQuery dataQuery = new BackendlessDataQuery();
                dataQuery.setWhereClause("business.objectId='" + business.getObjectId() + "'");
                return customer.find(dataQuery).getData();
            }

            @Override
            protected void onPostExecute(List<customer> o) {
                super.onPostExecute(o);

                ServiceResult result = new ServiceResult();
                result.success(o);

                handler.onComplete(result);
            }
        }.execute();
    }
}
