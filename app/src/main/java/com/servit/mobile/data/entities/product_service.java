package com.servit.mobile.data.entities;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

public class product_service
{
  private String description;
  private String objectId;
  private java.util.Date created;
  private String ownerId;
  private String cp;
  private Integer quantity;
  private String name;
  private java.util.Date updated;
  private String sp;
  private String image;
  private String desc;
  private String duration;
  private category category;
  private business business;

  @Override
  public boolean equals(Object o) {
    return this.objectId.equals(((product_service)o).objectId);
  }

  @Override
  public int hashCode() {
    return this.objectId.hashCode();
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription( String description )
  {
    this.description = description;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public String getCp()
  {
    return cp;
  }

  public void setCp( String cp )
  {
    this.cp = cp;
  }

  public Integer getQuantity()
  {
    return quantity;
  }

  public void setQuantity( Integer quantity )
  {
    this.quantity = quantity;
  }

  public String getName()
  {
    return name;
  }

  public void setName( String name )
  {
    this.name = name;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public String getSp()
  {
    return sp;
  }

  public void setSp( String sp )
  {
    this.sp = sp;
  }

  public String getImage()
  {
    return image;
  }

  public void setImage( String image )
  {
    this.image = image;
  }

  public String getDesc()
  {
    return desc;
  }

  public void setDesc( String desc )
  {
    this.desc = desc;
  }

  public String getDuration()
  {
    return duration;
  }

  public void setDuration( String duration )
  {
    this.duration = duration;
  }

  public category getCategory()
  {
    return category;
  }

  public void setCategory( category category )
  {
    this.category = category;
  }

  public business getBusiness()
  {
    return business;
  }

  public void setBusiness( business business )
  {
    this.business = business;
  }

                                                    
  public product_service save()
  {
    return Backendless.Data.of( product_service.class ).save( this );
  }

  public Future<product_service> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<product_service> future = new Future<product_service>();
      Backendless.Data.of( product_service.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<product_service> callback )
  {
    Backendless.Data.of( product_service.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( product_service.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( product_service.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( product_service.class ).remove( this, callback );
  }

  public static product_service findById( String id )
  {
    return Backendless.Data.of( product_service.class ).findById( id );
  }

  public static Future<product_service> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<product_service> future = new Future<product_service>();
      Backendless.Data.of( product_service.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<product_service> callback )
  {
    Backendless.Data.of( product_service.class ).findById( id, callback );
  }

  public static product_service findFirst()
  {
    return Backendless.Data.of( product_service.class ).findFirst();
  }

  public static Future<product_service> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<product_service> future = new Future<product_service>();
      Backendless.Data.of( product_service.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<product_service> callback )
  {
    Backendless.Data.of( product_service.class ).findFirst( callback );
  }

  public static product_service findLast()
  {
    return Backendless.Data.of( product_service.class ).findLast();
  }

  public static Future<product_service> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<product_service> future = new Future<product_service>();
      Backendless.Data.of( product_service.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<product_service> callback )
  {
    Backendless.Data.of( product_service.class ).findLast( callback );
  }

  public static BackendlessCollection<product_service> find(BackendlessDataQuery query )
  {
    return Backendless.Data.of( product_service.class ).find( query );
  }

  public static Future<BackendlessCollection<product_service>> findAsync(BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<product_service>> future = new Future<BackendlessCollection<product_service>>();
      Backendless.Data.of( product_service.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync(BackendlessDataQuery query, AsyncCallback<BackendlessCollection<product_service>> callback )
  {
    Backendless.Data.of( product_service.class ).find( query, callback );
  }
}