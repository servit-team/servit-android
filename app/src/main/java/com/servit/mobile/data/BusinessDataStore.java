package com.servit.mobile.data;

import com.servit.mobile.data.entities.business;
import com.servit.mobile.data.entities.product_service;

import java.util.List;

/**
 * Created by Brandon on 6/12/2016.
 */

public interface BusinessDataStore {
    void getBusinessById(IServiceHandler<business> handler);
    void loadProducts(final IServiceHandler<List<product_service>> handler);

    void addProduct(product_service newProduct, IServiceHandler<Void> handler);
    void saveBusiness(business business, IServiceHandler<Void> handler);
}
