package com.servit.mobile.data.entities;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;

public class address
{
  private String street_name2;
  private String postal_code;
  private String street_num;
  private String ownerId;
  private String province;
  private String city;
  private String street_name;
  private java.util.Date updated;
  private java.util.Date created;
  private String country;
  private String objectId;
  private GeoPoint geolocation;
  public String getStreet_name2()
  {
    return street_name2;
  }

  public void setStreet_name2( String street_name2 )
  {
    this.street_name2 = street_name2;
  }

  public String getPostal_code()
  {
    return postal_code;
  }

  public void setPostal_code( String postal_code )
  {
    this.postal_code = postal_code;
  }

  public String getStreet_num()
  {
    return street_num;
  }

  public void setStreet_num( String street_num )
  {
    this.street_num = street_num;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public String getProvince()
  {
    return province;
  }

  public void setProvince( String province )
  {
    this.province = province;
  }

  public String getCity()
  {
    return city;
  }

  public void setCity( String city )
  {
    this.city = city;
  }

  public String getStreet_name()
  {
    return street_name;
  }

  public void setStreet_name( String street_name )
  {
    this.street_name = street_name;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public String getCountry()
  {
    return country;
  }

  public void setCountry( String country )
  {
    this.country = country;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public GeoPoint getGeolocation()
  {
    return geolocation;
  }

  public void setGeolocation( GeoPoint geolocation )
  {
    this.geolocation = geolocation;
  }

                                                    
  public address save()
  {
    return Backendless.Data.of( address.class ).save( this );
  }

  public Future<address> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<address> future = new Future<address>();
      Backendless.Data.of( address.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<address> callback )
  {
    Backendless.Data.of( address.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( address.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( address.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( address.class ).remove( this, callback );
  }

  public static address findById( String id )
  {
    return Backendless.Data.of( address.class ).findById( id );
  }

  public static Future<address> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<address> future = new Future<address>();
      Backendless.Data.of( address.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<address> callback )
  {
    Backendless.Data.of( address.class ).findById( id, callback );
  }

  public static address findFirst()
  {
    return Backendless.Data.of( address.class ).findFirst();
  }

  public static Future<address> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<address> future = new Future<address>();
      Backendless.Data.of( address.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<address> callback )
  {
    Backendless.Data.of( address.class ).findFirst( callback );
  }

  public static address findLast()
  {
    return Backendless.Data.of( address.class ).findLast();
  }

  public static Future<address> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<address> future = new Future<address>();
      Backendless.Data.of( address.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<address> callback )
  {
    Backendless.Data.of( address.class ).findLast( callback );
  }

  public static BackendlessCollection<address> find(BackendlessDataQuery query )
  {
    return Backendless.Data.of( address.class ).find( query );
  }

  public static Future<BackendlessCollection<address>> findAsync(BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<address>> future = new Future<BackendlessCollection<address>>();
      Backendless.Data.of( address.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync(BackendlessDataQuery query, AsyncCallback<BackendlessCollection<address>> callback )
  {
    Backendless.Data.of( address.class ).find( query, callback );
  }
}