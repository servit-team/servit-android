package com.servit.mobile.data;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.servit.mobile.BuildConfig;
import com.servit.mobile.framework.UserSessionManager;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.ResponseBody;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.AuthenticationException;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by Brandon on 6/20/2016.
 */
public class PaymentDataStoreStripe implements PaymentDataStore {

    Gson gson = new GsonBuilder()
            .serializeNulls()
            .excludeFieldsWithoutExposeAnnotation()
            .create();

    OkHttpClient okHttpClient;

    Retrofit retrofit;

    NodeService restService;
    private Context context;

    {
        okHttpClient = new OkHttpClient();

        okHttpClient.interceptors().add(new Interceptor() {
            @Override
            public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("User-Agent", "Android")
                        .header("backendless-token", UserSessionManager.getInstance().getToken())
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.NODE_URL)
                .client(okHttpClient)
                .build();
        restService = retrofit.create(NodeService.class);
    }

    public PaymentDataStoreStripe(Context context){
        this.context = context;

        if (!BuildConfig.DEBUG) {
            retrofit.client().setReadTimeout(15, TimeUnit.SECONDS);
            retrofit.client().setWriteTimeout(15, TimeUnit.SECONDS);
        }
    }

    @Override
    public void getToken(Card card, final IServiceHandler<Token> handler) {
        if (handler == null){
            throw new IllegalArgumentException("handler must not be null");
        }

        try {
            Stripe stripe = new Stripe(BuildConfig.STRIPE_API_KEY);
            stripe.createToken(card, new TokenCallback() {
                @Override
                public void onError(Exception error) {
                    ServiceResult result = new ServiceResult();
                    result.fail(ErrorCode.SERVER_ERROR, null);

                    handler.onComplete(result);
                }

                @Override
                public void onSuccess(com.stripe.android.model.Token token) {
                    ServiceResult result = new ServiceResult();
                    result.success(token);

                    handler.onComplete(result);
                }
            });
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendToken(String email, String token, final IServiceHandler<Void> handler) {
        Call<ResponseBody> call = restService.postToken(email, token);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                ServiceResult result = new ServiceResult();
                result.success(null);

                handler.onComplete(result);
            }

            @Override
            public void onFailure(Throwable t) {
                ServiceResult result = new ServiceResult();
                result.fail(ErrorCode.SERVER_ERROR, t.getMessage());

                handler.onComplete(result);
            }
        });
    }

    private interface NodeService{
        @FormUrlEncoded
        @POST("/token")
        Call<ResponseBody> postToken(@Field("email") String email,@Field("token") String token);

    }
}
