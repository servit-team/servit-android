package com.servit.mobile.data.entities;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

public class customer
{
  private java.util.Date created;
  private java.util.Date updated;
  private String objectId;
  private String ownerId;

  private employee employee;
  private business business;
  private BackendlessUser user;
  public java.util.Date getCreated()
  {
    return created;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public String getOwnerId()
  {
    return ownerId;
  }


  public employee getEmployee()
  {
    return employee;
  }

  public void setEmployee( employee employee )
  {
    this.employee = employee;
  }

  public business getBusiness()
  {
    return business;
  }

  public void setBusiness( business business )
  {
    this.business = business;
  }



  public BackendlessUser getUser()
  {
    return user;
  }

  public void setUser( BackendlessUser user )
  {
    this.user = user;
  }

                                                    
  public customer save()
  {
    return Backendless.Data.of( customer.class ).save( this );
  }

  public Future<customer> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<customer> future = new Future<customer>();
      Backendless.Data.of( customer.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<customer> callback )
  {
    Backendless.Data.of( customer.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( customer.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( customer.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( customer.class ).remove( this, callback );
  }

  public static customer findById( String id )
  {
    return Backendless.Data.of( customer.class ).findById( id );
  }

  public static Future<customer> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<customer> future = new Future<customer>();
      Backendless.Data.of( customer.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<customer> callback )
  {
    Backendless.Data.of( customer.class ).findById( id, callback );
  }

  public static customer findFirst()
  {
    return Backendless.Data.of( customer.class ).findFirst();
  }

  public static Future<customer> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<customer> future = new Future<customer>();
      Backendless.Data.of( customer.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<customer> callback )
  {
    Backendless.Data.of( customer.class ).findFirst( callback );
  }

  public static customer findLast()
  {
    return Backendless.Data.of( customer.class ).findLast();
  }

  public static Future<customer> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<customer> future = new Future<customer>();
      Backendless.Data.of( customer.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<customer> callback )
  {
    Backendless.Data.of( customer.class ).findLast( callback );
  }

  public static BackendlessCollection<customer> find(BackendlessDataQuery query )
  {
    return Backendless.Data.of( customer.class ).find( query );
  }

  public static Future<BackendlessCollection<customer>> findAsync(BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<customer>> future = new Future<BackendlessCollection<customer>>();
      Backendless.Data.of( customer.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync(BackendlessDataQuery query, AsyncCallback<BackendlessCollection<customer>> callback )
  {
    Backendless.Data.of( customer.class ).find( query, callback );
  }
}