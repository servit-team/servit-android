package com.servit.mobile.data;

/**
 *  2/1/2016.
 */
public enum ErrorCode {
    SERVER_ERROR,
    NETWORK_ERROR,
    NO_INTERNET
}
