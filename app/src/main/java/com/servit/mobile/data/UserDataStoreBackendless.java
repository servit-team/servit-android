package com.servit.mobile.data;

import android.os.AsyncTask;
import android.util.Log;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.HeadersManager;
import com.backendless.IDataStore;
import com.backendless.exceptions.BackendlessException;
import com.backendless.persistence.BackendlessDataQuery;
import com.servit.mobile.data.entities.ServitUser;
import com.servit.mobile.data.entities.customer;
import com.servit.mobile.framework.Logger;
import com.servit.mobile.framework.UserSessionManager;

import java.util.List;

/**
 * Created by Brandon on 6/12/2016.
 */

public class UserDataStoreBackendless implements UserDataStore {
    @Override
    public void signupEmailPass(final String email, final String password, final IServiceHandler<Boolean> handler) {
        if (handler == null) {
            throw new IllegalArgumentException("handler must not be null");
        }

        new AsyncTask<Object, Integer, Boolean>() {
            @Override
            protected Boolean doInBackground(Object[] params) {
                ServitUser user = new ServitUser();

                user.setEmail(email);
                user.setPassword(password);
                user.setIsSignupCompleted(true);

                try {
                    Backendless.UserService.register(user);

                    return true;
                } catch (BackendlessException e) {
                    e.printStackTrace();

                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);

                ServiceResult result = new ServiceResult();
                if (aBoolean){
                    result.success(true);
                }
                else{
                    result.fail(ErrorCode.SERVER_ERROR, false);
                }

                handler.onComplete(result);
            }
        }.execute();
    }

    @Override
    public void signupName(final String firstName, final String lastName, final IServiceHandler<Void> handler) {
        if (handler == null) {
            throw new IllegalArgumentException("handler must not be null");
        }

        new AsyncTask<Object, Integer, Boolean>() {
            @Override
            protected Boolean doInBackground(Object[] params) {
                ServitUser user = new ServitUser();
                user.setFirstname(firstName);
                user.setLastname(lastName);
//                BackendlessUser backendlessUser = Backendless.UserService.register(user);
                return true;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                ServiceResult result = new ServiceResult();
                result.success(null);
                handler.onComplete(result);
            }
        }.execute();
    }

    @Override
    public void signupPhone(final String phone, final IServiceHandler<Void> handler) {
        if (handler == null) {
            throw new IllegalArgumentException("handler must not be null");
        }

        new AsyncTask<Object, Integer, Boolean>() {
            @Override
            protected Boolean doInBackground(Object[] params) {
                ServitUser user = new ServitUser();
                user.setPhone(phone);
//                BackendlessUser backendlessUser = Backendless.UserService.register(user);
                return true;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                ServiceResult result = new ServiceResult();
                result.success(null);
                handler.onComplete(result);
            }
        }.execute();
    }

    @Override
    public void login(final String email, final String password, final IServiceHandler<Boolean> handler) {
        if (handler == null) {
            throw new IllegalArgumentException("handler must not be null");
        }

        new AsyncTask<Object, Integer, Boolean>() {
            @Override
            protected Boolean doInBackground(Object[] params) {
                try {
                    BackendlessUser user = Backendless.UserService.login(email, password);
                    ServitUser servitUser = new ServitUser();
                    servitUser.setProperties(user.getProperties());
                    UserSessionManager.getInstance().setCurrentUser(HeadersManager.getInstance().getHeader(HeadersManager.HeadersEnum.USER_TOKEN_KEY), servitUser);
                    Log.i("d", user.getEmail());
                    return true;
                } catch (BackendlessException ex) {
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (!aBoolean) {
                    ServiceResult result = new ServiceResult();
                    result.fail(ErrorCode.NETWORK_ERROR, aBoolean);
                    handler.onComplete(result);
                } else {
                    ServiceResult result = new ServiceResult();
                    result.success(aBoolean);
                    handler.onComplete(result);
                }

            }
        }.execute();
    }

    @Override
    public void logout(final IServiceHandler<Void> handler) {
        if (handler == null) {
            throw new IllegalArgumentException("handler must not be null");
        }

        new AsyncTask<Object, Integer, Boolean>() {
            @Override
            protected Boolean doInBackground(Object[] params) {
                Backendless.UserService.logout();
                return true;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);

                ServiceResult result = new ServiceResult();
                result.success(null);

                handler.onComplete(result);
            }
        }.execute();
    }

    @Override
    public void getUserByEmail(String email, final IServiceHandler<ServitUser> handler) {
        if (handler == null) {
            throw new IllegalArgumentException("handler must not be null");
        }

        final BackendlessDataQuery query = new BackendlessDataQuery("email = '" + email + "'");

        final IDataStore<BackendlessUser> userDataStore = Backendless.Data.of(BackendlessUser.class);

        new AsyncTask<Object, Integer, BackendlessUser>() {
            @Override
            protected BackendlessUser doInBackground(Object[] params) {
                List<BackendlessUser> user = userDataStore.find(query).getData();

                if (user.size() == 1){
                    return user.get(0);
                }
                else{
                    return null;
                }
            }

            @Override
            protected void onPostExecute(BackendlessUser backendlessUser) {
                super.onPostExecute(backendlessUser);

                ServiceResult result = new ServiceResult();
                result.success(backendlessUser);

                handler.onComplete(result);
            }
        }.execute();

    }

    @Override
    public void createNewCustomerUser(final ServitUser servitUser, final IServiceHandler<ServitUser> handler) {
        if (handler == null) {
            throw new IllegalArgumentException("handler must not be null");
        }

        new AsyncTask<Object, Integer, customer>() {
            @Override
            protected customer doInBackground(Object[] params) {
                BackendlessUser user = new BackendlessUser();
                user.setProperty( "email", servitUser.getEmail() );
                user.setPassword( "test" );

                try {
                    BackendlessUser backendlessUser = Backendless.UserService.register(user);

                    if (backendlessUser != null) {
                        ServitUser newServitUser = ServitUser.fromBackendlessUser(backendlessUser);

                        newServitUser.setFirstname(servitUser.getFirstname());
                        newServitUser.setPhone(servitUser.getPhone());
                        newServitUser.setLastname(servitUser.getLastname());

                        Backendless.UserService.update(newServitUser);

                        customer customer = new customer();

                        customer.setUser(user);
                        customer.save();

                        return customer;
                    }
                }catch(Exception ex){
                    Logger.getInstance().error("Error creating new customer: " + ex.getMessage());

                    return null;
                }

                return null;
            }

            @Override
            protected void onPostExecute(customer customer) {
                super.onPostExecute(customer);

                ServiceResult result = new ServiceResult();
                if (customer != null) {
                    result.success(customer);
                }
                else{
                    result.fail(ErrorCode.NETWORK_ERROR, null);
                }

                handler.onComplete(result);
            }
        }.execute();
    }

    @Override
    public void sendCustomerEmail(final String subject, final String body, final String recipientEmail, final IServiceHandler<Void> handler) {
        if (handler == null) {
            throw new IllegalArgumentException("handler must not be null");
        }

        new AsyncTask<Object, Integer, Boolean>() {
            @Override
            protected Boolean doInBackground(Object[] params) {
                try {
                    Backendless.Messaging.sendHTMLEmail(subject, body, recipientEmail);
                } catch (Exception e) {
                    Logger.getInstance().error("Error sending email: " + e.getMessage());

                    return false;
                }

                return true;
            }

            @Override
            protected void onPostExecute(Boolean success) {
                super.onPostExecute(success);

                ServiceResult result = new ServiceResult();
                if (success) {
                    result.success(success);
                }
                else{
                    result.fail(ErrorCode.NETWORK_ERROR, null);
                }

                handler.onComplete(result);
            }
        }.execute();


    }


}
