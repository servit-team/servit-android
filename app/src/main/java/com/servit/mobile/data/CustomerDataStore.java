package com.servit.mobile.data;

import com.servit.mobile.data.entities.business;
import com.servit.mobile.data.entities.customer;

import java.util.List;

/**
 * Created by Brandon on 6/23/2016.
 */

public interface CustomerDataStore {
    void getCustomersForBusiness(business business, IServiceHandler<List<customer>> handler);

    void addCustomerForBusiness(business business, IServiceHandler<Void> iServiceHandler);
}
