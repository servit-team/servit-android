package com.servit.mobile.data.entities;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

public class settings
{
  private Boolean sales;
  private Boolean appointments;
  private String ownerId;
  private Boolean profile;
  private Boolean shop;
  private String objectId;
  private Boolean services;
  private java.util.Date updated;
  private java.util.Date created;
  private Boolean admin;
  private Boolean rewards;
  private Boolean goals;
  private BackendlessUser user;
  public Boolean getSales()
  {
    return sales;
  }

  public void setSales( Boolean sales )
  {
    this.sales = sales;
  }

  public Boolean getAppointments()
  {
    return appointments;
  }

  public void setAppointments( Boolean appointments )
  {
    this.appointments = appointments;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public Boolean getProfile()
  {
    return profile;
  }

  public void setProfile( Boolean profile )
  {
    this.profile = profile;
  }

  public Boolean getShop()
  {
    return shop;
  }

  public void setShop( Boolean shop )
  {
    this.shop = shop;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public Boolean getServices()
  {
    return services;
  }

  public void setServices( Boolean services )
  {
    this.services = services;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public Boolean getAdmin()
  {
    return admin;
  }

  public void setAdmin( Boolean admin )
  {
    this.admin = admin;
  }

  public Boolean getRewards()
  {
    return rewards;
  }

  public void setRewards( Boolean rewards )
  {
    this.rewards = rewards;
  }

  public Boolean getGoals()
  {
    return goals;
  }

  public void setGoals( Boolean goals )
  {
    this.goals = goals;
  }

  public BackendlessUser getUser()
  {
    return user;
  }

  public void setUser( BackendlessUser user )
  {
    this.user = user;
  }

                                                    
  public settings save()
  {
    return Backendless.Data.of( settings.class ).save( this );
  }

  public Future<settings> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<settings> future = new Future<settings>();
      Backendless.Data.of( settings.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<settings> callback )
  {
    Backendless.Data.of( settings.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( settings.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( settings.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( settings.class ).remove( this, callback );
  }

  public static settings findById( String id )
  {
    return Backendless.Data.of( settings.class ).findById( id );
  }

  public static Future<settings> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<settings> future = new Future<settings>();
      Backendless.Data.of( settings.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<settings> callback )
  {
    Backendless.Data.of( settings.class ).findById( id, callback );
  }

  public static settings findFirst()
  {
    return Backendless.Data.of( settings.class ).findFirst();
  }

  public static Future<settings> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<settings> future = new Future<settings>();
      Backendless.Data.of( settings.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<settings> callback )
  {
    Backendless.Data.of( settings.class ).findFirst( callback );
  }

  public static settings findLast()
  {
    return Backendless.Data.of( settings.class ).findLast();
  }

  public static Future<settings> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<settings> future = new Future<settings>();
      Backendless.Data.of( settings.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<settings> callback )
  {
    Backendless.Data.of( settings.class ).findLast( callback );
  }

  public static BackendlessCollection<settings> find(BackendlessDataQuery query )
  {
    return Backendless.Data.of( settings.class ).find( query );
  }

  public static Future<BackendlessCollection<settings>> findAsync(BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<settings>> future = new Future<BackendlessCollection<settings>>();
      Backendless.Data.of( settings.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync(BackendlessDataQuery query, AsyncCallback<BackendlessCollection<settings>> callback )
  {
    Backendless.Data.of( settings.class ).find( query, callback );
  }
}