package com.servit.mobile.data.entities;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

public class employee
{
  private java.util.Date created;
  private String email;
  private String ownerId;
  private String name;
  private String objectId;
  private java.util.Date updated;
  private java.util.List<customer> customer;
  private business business;
  public java.util.Date getCreated()
  {
    return created;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail( String email )
  {
    this.email = email;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public String getName()
  {
    return name;
  }

  public void setName( String name )
  {
    this.name = name;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public java.util.List<customer> getCustomer()
  {
    return customer;
  }

  public void setCustomer( java.util.List<customer> customer )
  {
    this.customer = customer;
  }

  public business getBusiness()
  {
    return business;
  }

  public void setBusiness( business business )
  {
    this.business = business;
  }

                                                    
  public employee save()
  {
    return Backendless.Data.of( employee.class ).save( this );
  }

  public Future<employee> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<employee> future = new Future<employee>();
      Backendless.Data.of( employee.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<employee> callback )
  {
    Backendless.Data.of( employee.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( employee.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( employee.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( employee.class ).remove( this, callback );
  }

  public static employee findById( String id )
  {
    return Backendless.Data.of( employee.class ).findById( id );
  }

  public static Future<employee> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<employee> future = new Future<employee>();
      Backendless.Data.of( employee.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<employee> callback )
  {
    Backendless.Data.of( employee.class ).findById( id, callback );
  }

  public static employee findFirst()
  {
    return Backendless.Data.of( employee.class ).findFirst();
  }

  public static Future<employee> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<employee> future = new Future<employee>();
      Backendless.Data.of( employee.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<employee> callback )
  {
    Backendless.Data.of( employee.class ).findFirst( callback );
  }

  public static employee findLast()
  {
    return Backendless.Data.of( employee.class ).findLast();
  }

  public static Future<employee> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<employee> future = new Future<employee>();
      Backendless.Data.of( employee.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<employee> callback )
  {
    Backendless.Data.of( employee.class ).findLast( callback );
  }

  public static BackendlessCollection<employee> find(BackendlessDataQuery query )
  {
    return Backendless.Data.of( employee.class ).find( query );
  }

  public static Future<BackendlessCollection<employee>> findAsync(BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<employee>> future = new Future<BackendlessCollection<employee>>();
      Backendless.Data.of( employee.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync(BackendlessDataQuery query, AsyncCallback<BackendlessCollection<employee>> callback )
  {
    Backendless.Data.of( employee.class ).find( query, callback );
  }
}