package com.servit.mobile.data.entities;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

public class user
{
  private String stripe_customer_id;
  private java.util.Date updated;
  private String objectId;
  private String email;
  private java.util.Date created;
  private String ownerId;
  public String getStripe_customer_id()
  {
    return stripe_customer_id;
  }

  public void setStripe_customer_id( String stripe_customer_id )
  {
    this.stripe_customer_id = stripe_customer_id;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail( String email )
  {
    this.email = email;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

                                                    
  public user save()
  {
    return Backendless.Data.of( user.class ).save( this );
  }

  public Future<user> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<user> future = new Future<user>();
      Backendless.Data.of( user.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<user> callback )
  {
    Backendless.Data.of( user.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( user.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( user.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( user.class ).remove( this, callback );
  }

  public static user findById( String id )
  {
    return Backendless.Data.of( user.class ).findById( id );
  }

  public static Future<user> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<user> future = new Future<user>();
      Backendless.Data.of( user.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<user> callback )
  {
    Backendless.Data.of( user.class ).findById( id, callback );
  }

  public static user findFirst()
  {
    return Backendless.Data.of( user.class ).findFirst();
  }

  public static Future<user> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<user> future = new Future<user>();
      Backendless.Data.of( user.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<user> callback )
  {
    Backendless.Data.of( user.class ).findFirst( callback );
  }

  public static user findLast()
  {
    return Backendless.Data.of( user.class ).findLast();
  }

  public static Future<user> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<user> future = new Future<user>();
      Backendless.Data.of( user.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<user> callback )
  {
    Backendless.Data.of( user.class ).findLast( callback );
  }

  public static BackendlessCollection<user> find(BackendlessDataQuery query )
  {
    return Backendless.Data.of( user.class ).find( query );
  }

  public static Future<BackendlessCollection<user>> findAsync(BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<user>> future = new Future<BackendlessCollection<user>>();
      Backendless.Data.of( user.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync(BackendlessDataQuery query, AsyncCallback<BackendlessCollection<user>> callback )
  {
    Backendless.Data.of( user.class ).find( query, callback );
  }
}