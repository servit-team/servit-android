package com.servit.mobile.data;

import com.servit.mobile.data.entities.ServitUser;

/**
 * Created by Brandon on 6/12/2016.
 */

public interface UserDataStore {
    void signupEmailPass(String email, String password, IServiceHandler<Boolean> handler);
    void signupName(String firstName, String lastName, IServiceHandler<Void> handler);
    void signupPhone(String phone, IServiceHandler<Void> handler);
    void login(String email, String password, IServiceHandler<Boolean> handler);
    void logout(IServiceHandler<Void> handler);
    void getUserByEmail(String email, IServiceHandler<ServitUser> handler);
    void createNewCustomerUser(final ServitUser servitUser, final IServiceHandler<ServitUser> handler);
    void sendCustomerEmail(final String subject, final String body, final String recipientEmail, final IServiceHandler<Void> handler);
}
