package com.servit.mobile.data.entities;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

public class owner
{
  private java.util.Date updated;
  private java.util.Date created;
  private String objectId;
  private String ownerId;
  private java.util.List<business> business;
  public java.util.Date getUpdated()
  {
    return updated;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public java.util.List<business> getBusiness()
  {
    return business;
  }

  public void setBusiness( java.util.List<business> business )
  {
    this.business = business;
  }

                                                    
  public owner save()
  {
    return Backendless.Data.of( owner.class ).save( this );
  }

  public Future<owner> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<owner> future = new Future<owner>();
      Backendless.Data.of( owner.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<owner> callback )
  {
    Backendless.Data.of( owner.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( owner.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( owner.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( owner.class ).remove( this, callback );
  }

  public static owner findById( String id )
  {
    return Backendless.Data.of( owner.class ).findById( id );
  }

  public static Future<owner> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<owner> future = new Future<owner>();
      Backendless.Data.of( owner.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<owner> callback )
  {
    Backendless.Data.of( owner.class ).findById( id, callback );
  }

  public static owner findFirst()
  {
    return Backendless.Data.of( owner.class ).findFirst();
  }

  public static Future<owner> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<owner> future = new Future<owner>();
      Backendless.Data.of( owner.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<owner> callback )
  {
    Backendless.Data.of( owner.class ).findFirst( callback );
  }

  public static owner findLast()
  {
    return Backendless.Data.of( owner.class ).findLast();
  }

  public static Future<owner> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<owner> future = new Future<owner>();
      Backendless.Data.of( owner.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<owner> callback )
  {
    Backendless.Data.of( owner.class ).findLast( callback );
  }

  public static BackendlessCollection<owner> find(BackendlessDataQuery query )
  {
    return Backendless.Data.of( owner.class ).find( query );
  }

  public static Future<BackendlessCollection<owner>> findAsync(BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<owner>> future = new Future<BackendlessCollection<owner>>();
      Backendless.Data.of( owner.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync(BackendlessDataQuery query, AsyncCallback<BackendlessCollection<owner>> callback )
  {
    Backendless.Data.of( owner.class ).find( query, callback );
  }
}