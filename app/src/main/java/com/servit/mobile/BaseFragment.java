package com.servit.mobile;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.servit.mobile.framework.Logger;
import com.servit.mobile.framework.annotations.FragmentContentView;
import com.servit.mobile.framework.annotations.InstanceState;
import com.servit.mobile.framework.annotations.ServiceLocator;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Brandon on 6/10/2016.
 */

public class BaseFragment extends Fragment{
    public static String TAG  = "BaseFragment";

    @Bind(R.id.toolbar)
    @Nullable
    protected Toolbar toolbar;

    private WeakReference<BaseActivity> baseActivity;

    private boolean isRestored;

    protected boolean getIsRestored(){
        return isRestored;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        injectDependencies();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        FragmentContentView annotation = ((Object) this).getClass().getAnnotation(FragmentContentView.class);

        View view = null;
        if (annotation != null && annotation.value() >= 0)
            view = inflater.inflate(annotation.value(), null, false);
        if (view == null)
            view = super.onCreateView(inflater, container, savedInstanceState);

        assert view != null;

        onRestoreView(savedInstanceState);

        setupToolbar();

        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof BaseActivity){
            baseActivity = new WeakReference<>((BaseActivity)context);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        baseActivity = null;
    }

    private void setupToolbar() {
        if (toolbar == null) return;

        if (getActivity() instanceof BaseActivity) {
            final BaseActivity baseActivity = ((BaseActivity) getActivity());

            baseActivity.setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    baseActivity.handleToolbarToggleClick();
                }
            });
        }
    }

    public BaseActivity getBaseActivity(){
        return baseActivity.get();
    }

    protected void setTitle(String title) {
        getActivity().setTitle(title);
    }

    protected void injectDependencies(){

    }

    protected <T> T resolve(Class<T> clazz){
        return ServiceLocator.getInstance().resolve(clazz);
    }

    @Override
    ///find all the member variables that are tagged with @InstanceState so that we can
    ///automatically persist them in the bundle
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        ArrayList<Field> fields = new ArrayList<>();

        //search the fragment and it's base classes to find all member variables
        //stop at basefragment to ignore all android level classes
        Class<?> i = this.getClass();
        while (i != null && i != Object.class) {
            fields.addAll(Arrays.asList(i.getDeclaredFields()));

            i = i != BaseFragment.class ? i.getSuperclass() : null;
        }

        // Get all fields of the object annotated for serialization
        if (fields.size() > 0) {
            for (Field f : fields) {
                InstanceState annotation = f.getAnnotation(InstanceState.class);

                if (annotation != null) {
                    try {
                        //private variables need to be marked accessible to be read
                        f.setAccessible(true);
                        outState.putSerializable(f.getName(), (Serializable)f.get(this));
                        f.setAccessible(false);
                    } catch (IllegalAccessException e) {
                        Logger.getInstance().error("onSaveInstanceState ", e);


                    }
                }
            }
        }
    }

    ///find all the member variables that are tagged with @InstanceState so that we can
    ///automatically restore them from the bundle
    private void onRestoreView(Bundle savedInstanceState){
        if (savedInstanceState != null) {
            isRestored = true;

            ArrayList<Field> fields = new ArrayList<>();

            //search the fragment and it's base classes to find all member variables
            //stop at basefragment to ignore all android level classes
            Class<?> i = this.getClass();
            while (i != null && i != Object.class) {
                fields.addAll(Arrays.asList(i.getDeclaredFields()));

                i = i != BaseFragment.class ? i.getSuperclass() : null;
            }

            // Get all fields of the object annotated for serialization
            if (fields.size() > 0) {
                for (Field f : fields) {
                    InstanceState annotation = f.getAnnotation(InstanceState.class);

                    if (annotation != null) {
                        try {
                            //restore the tagged member variable from the bundle
                            if (savedInstanceState.containsKey(f.getName())) {
                                //private variables need to be marked accessible to be read
                                f.setAccessible(true);
                                f.set(this, savedInstanceState.getSerializable(f.getName()));
                                f.setAccessible(false);
                            }
                        } catch (IllegalAccessException e) {
                            Logger.getInstance().error("Setting Field in onRestoreView:", e);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        ButterKnife.unbind(this);
    }
}
