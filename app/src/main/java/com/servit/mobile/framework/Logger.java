package com.servit.mobile.framework;

/**
 * Created by Brandon on 6/28/2016.
 */

public class Logger {
    private static com.backendless.logging.Logger logger = com.backendless.logging.Logger.getLogger("logger");

    public static com.backendless.logging.Logger getInstance(){
        return logger;
    }
}
