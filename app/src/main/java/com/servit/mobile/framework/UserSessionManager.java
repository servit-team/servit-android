package com.servit.mobile.framework;

import com.servit.mobile.data.entities.ServitUser;

/**
 * Created by Brandon on 6/12/2016.
 */

public class UserSessionManager {
    private static UserSessionManager userSessionManager = new UserSessionManager();
    private String userToken;
    private ServitUser user;

    public static UserSessionManager getInstance(){
        return userSessionManager;
    }

    public void setCurrentUser(String userToken, ServitUser user){
        this.userToken = userToken;
        this.user = user;
    }

    public void deleteSession(){
        this.userToken = null;
        this.user = null;
    }

    public ServitUser getCurrentUser(){
        return user;
    }
    public String getToken(){
        return userToken;
    }
}
