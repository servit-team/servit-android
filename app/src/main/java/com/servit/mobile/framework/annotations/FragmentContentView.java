package com.servit.mobile.framework.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Created by Brandon on 7/27/2015.
 */
@Target({ElementType.TYPE})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface FragmentContentView {
    int value();
}