package com.servit.mobile.framework.annotations;

import java.util.HashMap;

/**
 * Created by Brandon on 7/27/2015.
 */
public class ServiceLocator {
    private static ServiceLocator instance;

    private HashMap<Class<?>, Object> items;

    protected ServiceLocator(){
        items = new HashMap<>();
    }

    public static ServiceLocator getInstance(){
        if (instance == null){
            instance = new ServiceLocator();
        }

        return instance;
    }

    public <T> void put(Class<?> clazz, Object obj) {
        items.put(clazz, obj);
    }

    public <T> T resolve(Class<T> clazz){
        if (items.containsKey(clazz)){
            return (T)items.get(clazz);
        }
        else{
            return null;
        }
    }
}
