package com.servit.mobile.framework.annotations;

import com.servit.mobile.BaseFragment;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Created by Brandon on 6/10/2016.
 */
@Target({ElementType.TYPE})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface MenuActivity {
    Class<? extends BaseFragment> contentFragment();
}
