package com.servit.mobile.framework.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 *  12/15/2015.
 */
@Target({ElementType.FIELD})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface InstanceState {
}
