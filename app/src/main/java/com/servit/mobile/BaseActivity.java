package com.servit.mobile;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.servit.mobile.data.IServiceHandler;
import com.servit.mobile.data.ServiceResult;
import com.servit.mobile.data.UserDataStore;
import com.servit.mobile.framework.UserSessionManager;
import com.servit.mobile.framework.annotations.ActivityFragmentContent;
import com.servit.mobile.framework.annotations.ActivityView;
import com.servit.mobile.framework.annotations.MenuActivity;
import com.servit.mobile.framework.annotations.ServiceLocator;
import com.servit.mobile.modules.auth.AuthActivity;
import com.servit.mobile.modules.dashboard.DashboardActivity;
import com.servit.mobile.modules.goals.GoalsActivity;
import com.servit.mobile.modules.manage_business.EditBusinessActivity;
import com.servit.mobile.modules.profile.ProfileActivity;
import com.servit.mobile.modules.rewards.RewardsActivity;
import com.servit.mobile.modules.sales.SalesActivity;
import com.servit.mobile.modules.services.ServicesActivity;
import com.servit.mobile.modules.shop.ShopActivity;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Brandon on 6/9/2016.
 */

public abstract class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    final static String ACTIVITY_FRAGMENT_TAG = "activityFragmentTag";
    UserDataStore userDataStore;
    private int fragmentContainerId;

    protected ToolbarColorScheme toolbarColorScheme = ToolbarColorScheme.PRIMARY;

    public void handleToolbarToggleClick() {

    }

    protected enum ToolbarColorScheme {
        PRIMARY,
        SECONDARY
    }

    @Nullable
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    BaseFragment fragment;

    ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterManagers();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupFragmentAnnotations();
        checkForUpdates();

        MenuActivity menuActivityAnnotation = ((Object) this).getClass().getAnnotation(MenuActivity.class);

        if (menuActivityAnnotation != null) {
            setContentView(R.layout.activity_menu);

            try {
                fragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(ACTIVITY_FRAGMENT_TAG);

                if (fragment == null) {
                    Class<?> clazz = Class.forName(menuActivityAnnotation.contentFragment().getCanonicalName());

                    Method m = clazz.getMethod("getNewInstance");

                    fragment = (BaseFragment) m.invoke(null);

                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.add(R.id.fragment_content, fragment, ACTIVITY_FRAGMENT_TAG);
                    fragmentTransaction.commit();
                }

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        } else {
            ActivityView activityViewAnnotation = ((Object) this).getClass().getAnnotation(ActivityView.class);

            if (activityViewAnnotation != null && activityViewAnnotation.value() > 0) {
                setContentView(activityViewAnnotation.value());
            }
        }

        ButterKnife.bind(this);

        //add the menu to the toolbar if this is a menu activity
        if (menuActivityAnnotation != null) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerToggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                }
            };
            //drawer.setDrawerListener(toggle);
            mDrawerToggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
        }

        injectDependencies();

        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkForCrashes();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterManagers();

    }

    protected void replaceFragment(BaseFragment fragment, boolean addToBackStack, String tag) {
        FragmentTransaction transaction = createFragmentTransaction();

        if (tag == null) {
            tag = fragment.getClass().getName();
        }

        transaction.replace(R.id.fragment_content, fragment, tag);

        commitTransaction(transaction, addToBackStack, tag);

        fragment.setRetainInstance(true);
    }

    private FragmentTransaction createFragmentTransaction() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.activity_open_enter, R.anim.fade_out_fast, R.anim.fade_in_fast, R.anim.activity_close_exit);

        return transaction;
    }

    private void commitTransaction(FragmentTransaction transaction, boolean addToBackstack, String tag) {
        if (addToBackstack) {
            transaction.addToBackStack(tag);
        }

        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    private void setupFragmentAnnotations() {
        ActivityFragmentContent annotation = ((Object) this).getClass().getAnnotation(ActivityFragmentContent.class);

        if (annotation != null && annotation.value() >= 0) {
            fragmentContainerId = annotation.value();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return navigate(item.getItemId());
    }

    public boolean navigate(int id) {
        Intent intent = null;
        intent = getIntentById(id);

        if (intent != null) {
            startActivity(intent);
            return true;
        }
        return false;
    }

    private void logoutUser() {
        if (UserSessionManager.getInstance().getCurrentUser() != null) {
            finish();
            userDataStore.logout(new IServiceHandler<Void>() {
                @Override
                public void onComplete(ServiceResult<Void> result) {
                    Toast.makeText(getApplicationContext(), "You have been logged out!", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public boolean navigate(int id, Bundle b) {
        Intent intent = null;
        intent = getIntentById(id);

        if (intent != null) {
            if (b != null) {
                intent.putExtras(b);
            }
            startActivity(intent);
            return true;
        }
        return false;
    }

    private Intent getIntentById(int id) {
        Intent intent = null;
        switch (id) {
            case R.id.nav_home:
                intent = new Intent(this, DashboardActivity.class);
                break;
            case R.id.nav_shop:
                intent = new Intent(this, ShopActivity.class);
                break;
            case R.id.nav_manage:
                intent = new Intent(this, EditBusinessActivity.class);
                break;
            case R.id.nav_goals:
                intent = new Intent(this, GoalsActivity.class);
                break;
            case R.id.nav_profile:
                intent = new Intent(this, ProfileActivity.class);
                break;
            case R.id.nav_rewards:
                intent = new Intent(this, RewardsActivity.class);
                break;
            case R.id.nav_sales:
                intent = new Intent(this, SalesActivity.class);
                break;
            case R.id.nav_services:
                intent = new Intent(this, ServicesActivity.class);
                break;
            case R.id.nav_logout:
                logoutUser();
                intent = new Intent(this, AuthActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
        }
        return intent;
    }

    private void injectDependencies() {
        userDataStore = resolve(UserDataStore.class);
    }

    protected <T> T resolve(Class<T> clazz) {
        return ServiceLocator.getInstance().resolve(clazz);
    }

    public void showToolbarBackIcon() {
        if (toolbar == null) return;
        toolbar.setNavigationIcon(getBackIcon());
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private Drawable getBackIcon() {
        Drawable backArrow = getResources().getDrawable((R.drawable.md_nav_back));
        if (toolbarColorScheme == ToolbarColorScheme.PRIMARY) {
            backArrow.setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        } else {
            backArrow.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        }
        return backArrow;
    }

    private void checkForCrashes() {
        CrashManager.register(this);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }

}
