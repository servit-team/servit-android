package com.servit.mobile.controls;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;

/**
 * Created by Brandon on 6/13/2016.
 */

public class HeaderCardView extends CardView {
    public HeaderCardView(Context context) {
        super(context);
    }

    public HeaderCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HeaderCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


}
