package com.servit.mobile.controls;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Brandon on 6/10/2016.
 */

public class ServitTextView extends TextView {
    public ServitTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ServitTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ServitTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
//        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/lifie.ttf");
//        setTypeface(tf);
    }
}
