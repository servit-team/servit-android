package com.servit.mobile.modules.cart;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.braintreepayments.cardform.view.CardEditText;
import com.braintreepayments.cardform.view.CardForm;
import com.braintreepayments.cardform.view.CvvEditText;
import com.braintreepayments.cardform.view.ExpirationDateEditText;
import com.dd.processbutton.iml.ActionProcessButton;
import com.servit.mobile.BaseFragment;
import com.servit.mobile.R;
import com.servit.mobile.data.IServiceHandler;
import com.servit.mobile.data.PaymentDataStore;
import com.servit.mobile.data.ServiceResult;
import com.servit.mobile.framework.UserSessionManager;
import com.servit.mobile.framework.annotations.FragmentContentView;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import butterknife.Bind;
import butterknife.OnClick;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

/**
 * Created by Brandon on 6/20/2016.
 */

@FragmentContentView(R.layout.fragment_payment)
public class PaymentFragment extends BaseFragment {

    private static final int MY_SCAN_REQUEST_CODE = 786;
    @Bind(R.id.bt_card_form)
    CardForm cardForm;

    @Bind(R.id.submit)
    ActionProcessButton btnSubmit;

    @Bind(R.id.bt_card_form_card_number)
    CardEditText cardEditText;

    @Bind(R.id.bt_card_form_expiration)
    ExpirationDateEditText expirationDateEditText;

    @Bind(R.id.bt_card_form_cvv)
    CvvEditText cvvEditText;

    PaymentDataStore paymentDataStore;

    public static PaymentFragment getInstance() {
        return new PaymentFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        cardForm.setRequiredFields(getActivity(), true, true, true, false, "Purchase");


        //mock data
        cardEditText.setText("4242-4242-4242-4242");
        expirationDateEditText.setText("122017");
        cvvEditText.setText("123");

        cardEditText.setHint("4242-4242-4242-4242");
        cardEditText.setTextColor(getResources().getColor(R.color.black));
        expirationDateEditText.setHint("122017");
        expirationDateEditText.setTextColor(getResources().getColor(R.color.black));
        cvvEditText.setHint("123");
        cvvEditText.setTextColor(getResources().getColor(R.color.black));

        cardForm.setGravity(Gravity.CENTER_VERTICAL);

        return view;
    }

    @OnClick(R.id.btn_scan_card)
    public void onScanPress(View v) {
        Intent scanIntent = new Intent(this.getContext(), CardIOActivity.class);
        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false

        // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MY_SCAN_REQUEST_CODE) {
            String resultDisplayStr;
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                resultDisplayStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";
                cardEditText.setText(scanResult.cardNumber);

                // Do something with the raw number, e.g.:
                // myService.setCardNumber( scanResult.cardNumber );

                if (scanResult.isExpiryValid()) {
                    resultDisplayStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";
                    expirationDateEditText.setText(scanResult.expiryMonth+"20"+scanResult.expiryYear);
                }

                if (scanResult.cvv != null) {
                    // Never log or display a CVV
                    resultDisplayStr += "CVV has " + scanResult.cvv.length() + " digits.\n";
                    cvvEditText.setText(scanResult.cvv);
                }

                if (scanResult.postalCode != null) {
                    resultDisplayStr += "Postal Code: " + scanResult.postalCode + "\n";
                }
                Log.d("PaymentFragment", resultDisplayStr);

            } else {
                resultDisplayStr = "Scan was canceled.";
                Log.d("PaymentFragment", resultDisplayStr);
            }
            // do something with resultDisplayStr, maybe display it in a textView
            // resultTextView.setText(resultDisplayStr);
        }
        // else handle other activity results
    }

    @OnClick(R.id.submit)
    void onSubmitPayment() {
        Card card = new Card(cardForm.getCardNumber(), Integer.parseInt(cardForm.getExpirationMonth()), Integer.parseInt(cardForm.getExpirationYear()), cardForm.getCvv());

        if (!card.validateCard()) {

            // Show errors
        } else {
            paymentDataStore.getToken(card, new IServiceHandler<Token>() {
                @Override
                public void onComplete(ServiceResult<Token> result) {
                    final String token = result.getResult().getId();
                    paymentDataStore.sendToken(UserSessionManager.getInstance().getCurrentUser().getEmail(), token, new IServiceHandler<Void>() {
                        @Override
                        public void onComplete(ServiceResult<Void> result) {
                            if (!result.getHasError()) {
                                Toast.makeText(getActivity(), "Succesfully uploaded token: " + token, Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity(), "Error uploadeding token: " + result.getErrorReason(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            });
        }
    }

    @Override
    protected void injectDependencies() {
        super.injectDependencies();

        paymentDataStore = resolve(PaymentDataStore.class);
    }
}
