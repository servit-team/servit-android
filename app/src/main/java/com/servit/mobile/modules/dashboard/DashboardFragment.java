package com.servit.mobile.modules.dashboard;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.servit.mobile.BaseFragment;
import com.servit.mobile.R;
import com.servit.mobile.data.IServiceHandler;
import com.servit.mobile.data.ServiceResult;
import com.servit.mobile.data.UserDataStore;
import com.servit.mobile.framework.annotations.FragmentContentView;

import butterknife.OnClick;

/**
 * Created by Brandon on 6/10/2016.
 */

@FragmentContentView(R.layout.fragment_home)
public class DashboardFragment extends BaseFragment {

    UserDataStore userDataStore;
    DashboardListener listener;
    Context mcontext;

    @OnClick(R.id.make_sale)
    void makeSale() {
//        navigateToNewSale();
        getBaseActivity().navigate(R.id.nav_shop);
    }

    @OnClick(R.id.sales)
    void sales() {
//        navigateToSales();
        getBaseActivity().navigate(R.id.nav_sales);
    }

    @OnClick(R.id.rewards)
    void rewards() {
//        navigateToRewards();
        getBaseActivity().navigate(R.id.nav_rewards);
    }

    public static DashboardFragment getNewInstance() {
        return new DashboardFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        injectDependencies();
        mcontext = getContext();
        return view;
    }

    @Override
    protected void injectDependencies() {
        super.injectDependencies();
        userDataStore = resolve(UserDataStore.class);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mcontext = context;
        listener = (DashboardListener) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    interface DashboardListener {
        void navigateToLogin();

        void navigateToSales();

        void navigateToRewards();

        void navigateToNewSale();
    }

    void logout() {
        userDataStore.logout(new IServiceHandler<Void>() {
            @Override
            public void onComplete(ServiceResult<Void> result) {
                Toast.makeText(getContext(), "Signed out!", Toast.LENGTH_SHORT).show();
                navigateToLogin();
            }
        });
    }

    private void navigateToLogin() {
        listener.navigateToLogin();
    }

    private void navigateToSales() {
        listener.navigateToSales();
    }

    private void navigateToRewards() {
        listener.navigateToRewards();
    }

    private void navigateToNewSale() {
        listener.navigateToNewSale();
    }
}
