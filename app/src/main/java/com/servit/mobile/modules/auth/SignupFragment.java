package com.servit.mobile.modules.auth;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.internal.MDButton;
import com.dd.processbutton.iml.ActionProcessButton;
import com.servit.mobile.BaseFragment;
import com.servit.mobile.R;
import com.servit.mobile.data.IServiceHandler;
import com.servit.mobile.data.ServiceResult;
import com.servit.mobile.data.UserDataStore;
import com.servit.mobile.framework.annotations.FragmentContentView;

import butterknife.Bind;

/**
 * Created by Brandon on 6/10/2016.
 */

@FragmentContentView(R.layout.fragment_signup)
public class SignupFragment extends BaseFragment {

    @Bind(R.id.btnLogin)
    MDButton btnLogin;

    @Bind(R.id.btnSignup)
    ActionProcessButton btnSignup;

    UserDataStore userDataStore;

    SignupListener listener;
    Handler handler;
    ViewGroup cardholder;
    View cardViewEmailPass;
    View cardViewFirstLastName;
    View cardViewPhone;

    private enum SignupState {EMAILPASS, NAME, PHONE, DONE}

    SignupState signupState;

    public static SignupFragment getNewInstance() {
        return new SignupFragment();
    }

    @Override
    protected void injectDependencies() {
        super.injectDependencies();

        userDataStore = resolve(UserDataStore.class);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        //won't match parent to activity if we don't do this
        ViewGroup.LayoutParams contentViewLayout = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(contentViewLayout);
        handler = new Handler();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        cardholder = (ViewGroup) view.findViewById(R.id.signup_cardholder);
        cardViewEmailPass = View.inflate(getContext(), R.layout.signup_emailpass, null);
        cardViewFirstLastName = View.inflate(getContext(), R.layout.signup_firstlastname, null);
        cardViewPhone = View.inflate(getContext(), R.layout.signup_phone, null);

        signupState = SignupState.EMAILPASS;
        nextCard(signupState);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onLogin();
            }
        });

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSignup(signupState);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        btnSignup.setProgress(0);
    }

    @Override
    public void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            btnSignup.setProgress(0);

        }
    };

    private Runnable runnablesignup = new Runnable() {
        @Override
        public void run() {
            switch (signupState) {
                case EMAILPASS:
                    Toast.makeText(getActivity(), "Great! Let's get your name now.", Toast.LENGTH_SHORT).show();
                    signupState = SignupState.NAME;
                    nextCard(signupState);
                    break;
                case NAME:
                    Toast.makeText(getActivity(), "Great! Let's get your name now.", Toast.LENGTH_SHORT).show();
                    signupState = SignupState.PHONE;
                    nextCard(signupState);
                    break;
                case PHONE:
                    Toast.makeText(getActivity(), "Great! Let's get your name now.", Toast.LENGTH_SHORT).show();
                    signupState = SignupState.DONE;
                    nextCard(signupState);
                    break;
                case DONE:
                    signupState = SignupState.DONE;
                    nextCard(signupState);
                default:
                    nextCard(signupState);
                    break;
            }

        }
    };

    private void nextCard(SignupState signupState) {
        cardholder.removeAllViews();
        switch (signupState) {
            case EMAILPASS:
                btnSignup.setProgress(0);
                cardholder.addView(cardViewEmailPass);
                break;
            case NAME:
                btnSignup.setProgress(0);
                cardholder.addView(cardViewFirstLastName);
                break;
            case PHONE:
                btnSignup.setProgress(0);
                cardholder.addView(cardViewPhone);
                View v = getView();
                TextView txtPhoneNumber = (TextView) v.findViewById(R.id.phone);
                txtPhoneNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
                break;
            case DONE:
                Toast.makeText(getActivity(), "All Done. Let's Get to Business.", Toast.LENGTH_LONG).show();
                btnSignup.setProgress(0);
                navigateToHome();
                break;
        }
    }


    private void onLogin() {
        getActivity().onBackPressed();
    }

    private void navigateToHome() {
        listener.navigateToHome();
    }

    interface SignupListener {

        void navigateToHome();

    }

    void attemptSignup(SignupState signupState) {
        boolean hasError = false;
        View v = getView();
        switch (signupState) {
            case EMAILPASS:
                EditText txtEmail = (EditText) v.findViewById(R.id.email);
                EditText txtPassword = (EditText) v.findViewById(R.id.password);
                String email = txtEmail.getText().toString();
                String password = txtPassword.getText().toString();
                if (!isEmailValid(email)) {
                    txtEmail.setError("Invalid email address");
                    hasError = true;
                }
                if (!isPasswordValid(password)) {
                    txtPassword.setError("Password must be more then 4 characters");
                    hasError = true;
                }
                if (hasError) {
                    btnSignup.setProgress(-1);
                    handler.postDelayed(runnable, 1000);
                    return;
                }
                btnSignup.setProgress(1);
                userDataStore.signupEmailPass(txtEmail.getText().toString(), txtPassword.getText().toString(), new IServiceHandler<Boolean>() {
                    @Override
                    public void onComplete(ServiceResult<Boolean> result) {
                        if (!result.getHasError()) {
                            btnSignup.setProgress(100);
                            handler.postDelayed(runnablesignup, 1500);
                        }
                        else{
                            Toast.makeText(getActivity(), "User already exists", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                break;
            case NAME:
                EditText txtFirstName = (EditText) v.findViewById(R.id.firstname);
                EditText txtLastName = (EditText) v.findViewById(R.id.lastname);
                if (txtFirstName.length() == 0) {
                    txtFirstName.setError("Please enter first name");
                    hasError = true;
                }

                if (txtLastName.length() == 0) {
                    txtLastName.setError("Please enter last name");
                    hasError = true;
                }
                if (hasError) {
                    btnSignup.setProgress(-1);
                    handler.postDelayed(runnable, 1000);
                    return;
                }
                btnSignup.setProgress(1);
                userDataStore.signupName(txtFirstName.getText().toString(), txtLastName.getText().toString(), new IServiceHandler<Void>() {
                    @Override
                    public void onComplete(ServiceResult<Void> result) {
                        btnSignup.setProgress(100);
                        handler.postDelayed(runnablesignup, 1500);
                    }
                });
                break;
            case PHONE:
                EditText txtPhoneNumber = (EditText) v.findViewById(R.id.phone);
                if (txtPhoneNumber.length() < 10) {
                    txtPhoneNumber.setError("Invalid phone number");
                    hasError = true;
                }
                if (hasError) {
                    btnSignup.setProgress(-1);
                    handler.postDelayed(runnable, 1000);
                    return;
                }
                btnSignup.setProgress(1);
                userDataStore.signupPhone(txtPhoneNumber.getText().toString(), new IServiceHandler<Void>() {
                    @Override
                    public void onComplete(ServiceResult<Void> result) {
                        btnSignup.setProgress(100);
                        handler.postDelayed(runnablesignup, 1500);
                    }
                });
                break;
            default:
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (SignupFragment.SignupListener) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= 3;
    }
}
