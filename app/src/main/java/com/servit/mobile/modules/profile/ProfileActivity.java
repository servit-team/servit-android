package com.servit.mobile.modules.profile;

import android.os.Bundle;

import com.servit.mobile.BaseActivity;
import com.servit.mobile.framework.annotations.MenuActivity;

@MenuActivity(contentFragment = ProfileFragment.class)
public class ProfileActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
