package com.servit.mobile.modules.sales;

import android.os.Bundle;

import com.servit.mobile.BaseActivity;
import com.servit.mobile.R;
import com.servit.mobile.framework.annotations.MenuActivity;

@MenuActivity(contentFragment = SalesFragment.class)
public class SalesActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.sales);
    }
}
