package com.servit.mobile.modules.manage_business.products;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.servit.mobile.BaseFragment;
import com.servit.mobile.R;
import com.servit.mobile.data.BusinessDataStore;
import com.servit.mobile.data.IServiceHandler;
import com.servit.mobile.data.ServiceResult;
import com.servit.mobile.data.entities.product_service;
import com.servit.mobile.framework.annotations.FragmentContentView;
import com.servit.mobile.modules.manage_business.EditBusinessActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by Brandon on 6/10/2016.
 */

@FragmentContentView(R.layout.fragment_products)
public class ProductsFragment extends BaseFragment {

    @Bind(R.id.recycler)
    EasyRecyclerView recyclerView;

    ProductAdapter productAdapter;

    BusinessDataStore businessDataStore;

    AddProductListener addProductListener;

    public static ProductsFragment getNewInstance(){
        return new ProductsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        if (savedInstanceState == null) {
            setupRecycler();
            loadProducts();
        }

        return view;
    }

    @Override
    protected void injectDependencies() {
        super.injectDependencies();

        businessDataStore = resolve(BusinessDataStore.class);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof EditBusinessActivity){
            addProductListener = (AddProductListener)context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        addProductListener = null;
    }

    @OnClick(R.id.add_product)
    void addProduct(){
        addProductListener.onAddProduct();
    }

    private void loadProducts() {
        businessDataStore.loadProducts(new IServiceHandler<List<product_service>>() {
            @Override
            public void onComplete(ServiceResult<List<product_service>> result) {
                if (!result.getHasError()) {
                    productAdapter.addAll(result.getResult());
                    productAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void setupRecycler() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        productAdapter = new ProductAdapter(getActivity());
        recyclerView.setAdapter(productAdapter);
    }

    public interface AddProductListener{
        void onAddProduct();
    }



    public class ProductAdapter extends RecyclerArrayAdapter<product_service> {
        public ProductAdapter(Context context) {
            super(context);
        }

        @Override
        public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            return new ProductViewHolder(parent);
        }
    }
}
