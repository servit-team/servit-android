package com.servit.mobile.modules.manage_business;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.servit.mobile.R;
import com.servit.mobile.data.BusinessDataStore;
import com.servit.mobile.data.entities.business;
import com.servit.mobile.BaseFragment;
import com.servit.mobile.framework.UserSessionManager;
import com.servit.mobile.framework.annotations.FragmentContentView;
import com.servit.mobile.framework.annotations.InstanceState;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by Brandon on 6/10/2016.
 */

@FragmentContentView(R.layout.fragment_manage)
public class ManageBusinessFragment extends BaseFragment {

    BusinessDataStore businessDataStore;

    @InstanceState
    business business;

    @Bind(R.id.name)
    TextView txtName;

    @Bind(R.id.address)
    TextView txtAddress;

    @Bind(R.id.address2)
    TextView txtAddress2;

    @Bind(R.id.postal)
    TextView txtPostal;

    @Bind(R.id.image)
    ImageView imageLogo;

    ManageClickListener manageClickListner;

    public static ManageBusinessFragment getNewInstance(){
        return new ManageBusinessFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        setTitle(getString(R.string.manage_business));

        business = UserSessionManager.getInstance().getCurrentUser().getOwner().getBusiness().get(0);

        Picasso.with(getActivity()).load(Uri.parse(business.getLogo())).into(imageLogo);
        txtName.setText(business.getName());
        txtAddress.setText(business.getAddress().getStreet_num() + " " + business.getAddress().getStreet_name() + " " + business.getAddress().getStreet_name2());
        txtAddress2.setText(business.getAddress().getCity() + " " + business.getAddress().getProvince() + " " + business.getAddress().getCity() + " " + business.getAddress().getCountry());
        txtPostal.setText(business.getAddress().getPostal_code());

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_manage_edit, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.edit){
            manageClickListner.onEdit(business);
            return true;
        }

        return false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof ManageClickListener){
            manageClickListner = (ManageClickListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        manageClickListner = null;
    }

    @OnClick(R.id.employees)
    void employeesClicked(){
        manageClickListner.onEmployee();

    }

    @OnClick(R.id.products)
    void productsClicked(){
        manageClickListner.onProducts();

    }

    @Override
    protected void injectDependencies() {
        super.injectDependencies();

        businessDataStore = resolve(BusinessDataStore.class);
    }

    public interface ManageClickListener {
        void onEmployee();
        void onProducts();
        void onEdit(business business);
    }
}
