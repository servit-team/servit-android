package com.servit.mobile.modules.cart;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dd.processbutton.FlatButton;
import com.dd.processbutton.iml.ActionProcessButton;
import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.servit.mobile.BaseFragmentListener;
import com.servit.mobile.R;
import com.servit.mobile.data.entities.product_service;
import com.servit.mobile.framework.annotations.FragmentContentView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by Brandon on 6/13/2016.
 */
@FragmentContentView(R.layout.fragment_cart)
public class CartFragment extends BaseFragmentListener<CartFragment.OnPaymentListener> {
    @Bind(R.id.recycler)
    EasyRecyclerView recyclerView;

    @Bind(R.id.cart_empty)
    View cartEmpty;

    @Bind(R.id.total)
    TextView txtTotal;

    @Bind(R.id.btnPay)
    ActionProcessButton btnPay;

    @Nullable
    @Bind(R.id.btnMinus)
    FlatButton btnMinus;

    @Bind(R.id.btnCustomer)
    View btnCustomer;

    ProductAdapter adapter;

    public interface OnPaymentListener {
        void onPay();
        void onSelectCustomer();
        void onAddCustomer();
    }

    public static CartFragment getNewInstance() {
        return new CartFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        bindRecycler();

        return view;
    }

    @OnClick(R.id.btnPay)
    void onPay() {
        listener.onPay();
    }

    private void bindRecycler() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new ProductAdapter(getActivity());
        adapter.addAll(getShoppingItems(ShoppingCart.getInstance().getProducts()));
        recyclerView.setAdapter(adapter);


        if (adapter.getCount() > 0) {
            cartEmpty.setVisibility(View.GONE);
            txtTotal.setVisibility(View.VISIBLE);

            if (btnMinus != null) {
                btnMinus.setVisibility(View.VISIBLE);
            }

        } else {
            cartEmpty.setVisibility(View.VISIBLE);
            txtTotal.setVisibility(View.GONE);

            if (btnMinus != null) {
                btnMinus.setVisibility(View.GONE);
            }
        }


        //calculate total price
        double total = 0;

        for (ShoppingCartItem item : adapter.getAllData()) {
            total += item.quantity * Double.parseDouble(item.product.getSp());
        }

        txtTotal.setText(String.valueOf(total));
    }

    @OnClick(R.id.btnCustomer)
    void onAddCustomer(){
        listener.onAddCustomer();
    }

    private List<ShoppingCartItem> getShoppingItems(HashMap<product_service, Integer> products) {
        List<ShoppingCartItem> shoppintItems = new ArrayList<>();

        for (product_service product : products.keySet()) {
            shoppintItems.add(new ShoppingCartItem(product, products.get(product)));
        }

        return shoppintItems;
    }

    class ShoppingCartItem {
        public ShoppingCartItem(product_service product, int quantity) {
            this.product = product;
            this.quantity = quantity;
        }

        public product_service product;
        public int quantity;
    }


    public class ProductAdapter extends RecyclerArrayAdapter<ShoppingCartItem> {
        public ProductAdapter(Context context) {
            super(context);
        }

        @Override
        public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            return new ProductViewHolder(parent);
        }
    }

    public class ProductViewHolder extends BaseViewHolder<ShoppingCartItem> {

        TextView txtName;
        TextView txtQuantity;
        TextView txtSalePrice;
        FlatButton btnMinus;
        FlatButton btnPlus;

        public ProductViewHolder(ViewGroup parent) {
            super(parent, R.layout.recycler_row_cart_product);

            txtName = $(R.id.name);
            txtQuantity = $(R.id.quantity);
            txtSalePrice = $(R.id.sale_price);
            btnMinus = $(R.id.btnMinus);
            btnPlus = $(R.id.btnPlus);
        }

        @Override
        public void setData(final ShoppingCartItem shoppingCartItem) {
            txtName.setText(getContext().getString(R.string.name_header) + shoppingCartItem.product.getName());
            txtQuantity.setText(getContext().getString(R.string.quantity_header) + shoppingCartItem.quantity);
            txtSalePrice.setText(getContext().getString(R.string.sale_price_header) + shoppingCartItem.product.getSp());

            btnMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShoppingCart.getInstance().removeItem(shoppingCartItem.product);
                    bindRecycler();
                }
            });
            btnPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShoppingCart.getInstance().addItem(shoppingCartItem.product);
                    bindRecycler();
                }
            });
        }
    }
}
