package com.servit.mobile.modules.goals;

import android.os.Bundle;

import com.servit.mobile.BaseActivity;
import com.servit.mobile.framework.annotations.MenuActivity;

@MenuActivity(contentFragment = GoalsFragment.class)
public class GoalsActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
