package com.servit.mobile.modules.shop;

import android.os.Bundle;

import com.servit.mobile.BaseActivity;
import com.servit.mobile.R;
import com.servit.mobile.data.entities.business;
import com.servit.mobile.framework.annotations.MenuActivity;
import com.servit.mobile.modules.cart.CartFragment;
import com.servit.mobile.modules.cart.PaymentFragment;
import com.servit.mobile.modules.customer.AddCustomerFragment;
import com.servit.mobile.modules.customer.FindCustomerFragment;

@MenuActivity(contentFragment = ShopFragment.class)
public class ShopActivity extends BaseActivity implements ShopFragment.ShopClickListener, CartFragment.OnPaymentListener{
    private static final String CART_FRAGMENT = "cartFragment";
    private static final String PAYMENT_FRAGMENT = "paymentFragment";
    private static final String ADD_CUSTOMER_FRAGMENT = "addCustomerFragment";
    private static final String FIND_CUSTOMER_FRAGMENT = "findCustomerFragment";

    business business;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null){
            business = (business) savedInstanceState.get("business");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable("business", business);
    }

    @Override
    public void onShop() {
        setTitle(getString(R.string.shopping_cart));
        replaceFragment(CartFragment.getNewInstance(), true, CART_FRAGMENT);
    }

    @Override
    public void onPay() {
        replaceFragment(PaymentFragment.getInstance(), true, PAYMENT_FRAGMENT);
    }

    @Override
    public void onSelectCustomer() {
        replaceFragment(FindCustomerFragment.getNewInstance(business), true, FIND_CUSTOMER_FRAGMENT);
    }

    @Override
    public void onAddCustomer() {
        replaceFragment(AddCustomerFragment.getNewInstance(business), true, ADD_CUSTOMER_FRAGMENT);
    }
}
