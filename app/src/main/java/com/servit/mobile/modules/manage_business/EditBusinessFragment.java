package com.servit.mobile.modules.manage_business;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.servit.mobile.R;
import com.servit.mobile.data.BusinessDataStore;
import com.servit.mobile.data.IServiceHandler;
import com.servit.mobile.data.ServiceResult;
import com.servit.mobile.data.entities.business;
import com.servit.mobile.BaseFragment;
import com.servit.mobile.framework.annotations.FragmentContentView;
import com.servit.mobile.framework.annotations.InstanceState;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by Brandon on 6/10/2016.
 */

@FragmentContentView(R.layout.fragment_manage_edit)
public class EditBusinessFragment extends BaseFragment {
    BusinessDataStore businessDataStore;

    @InstanceState
    private business business;

    @Bind(R.id.image)
    ImageView imageLogo;

    @Bind(R.id.business_name)
    TextView txtBusinessName;

    @Bind(R.id.phone_number)
    TextView txtPhoneNumber;

    @Bind(R.id.street_name)
    TextView txtStreetName;

    @Bind(R.id.street_name2)
    TextView txtStreetName2;

    @Bind(R.id.street_number)
    TextView txtStreetNumber;

    @Bind(R.id.city)
    TextView txtCity;

    @Bind(R.id.province)
    TextView txtProvince;

    @Bind(R.id.country)
    TextView txtCountry;

    @Bind(R.id.postal)
    TextView txtPostal;

    public static EditBusinessFragment getNewInstance(business business){
        EditBusinessFragment editBusinessFragment = new EditBusinessFragment();
        editBusinessFragment.business = business;
        return editBusinessFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        Picasso.with(getActivity()).load(Uri.parse(business.getLogo())).into(imageLogo);
        txtBusinessName.setText(business.getName());
        txtPhoneNumber.setText(business.getPhone());
        txtStreetName.setText(business.getAddress().getStreet_num());
        txtStreetName2.setText(business.getAddress().getStreet_name2());
        txtStreetNumber.setText(business.getAddress().getStreet_num());
        txtCity.setText(business.getAddress().getCity());
        txtProvince.setText(business.getAddress().getProvince());
        txtCountry.setText(business.getAddress().getCountry());
        txtPostal.setText(business.getAddress().getPostal_code());

        return view;
    }

    @Override
    protected void injectDependencies() {
        super.injectDependencies();

        businessDataStore = resolve(BusinessDataStore.class);
    }

    @OnClick(R.id.upload)
    void upload(){

    }

    @OnClick(R.id.save)
    void save(){
        business.setName(txtBusinessName.getText().toString());
        business.setPhone(txtPhoneNumber.getText().toString());
        business.getAddress().setStreet_name(txtStreetName.getText().toString());
        business.getAddress().setStreet_name2(txtStreetName2.getText().toString());
        business.getAddress().setStreet_num(txtStreetNumber.getText().toString());
        business.getAddress().setCity(txtCity.getText().toString());
        business.getAddress().setProvince(txtProvince.getText().toString());
        business.getAddress().setCountry(txtCountry.getText().toString());
        business.getAddress().setPostal_code(txtPostal.getText().toString());

        businessDataStore.saveBusiness(business, new IServiceHandler<Void>() {
            @Override
            public void onComplete(ServiceResult<Void> result) {
                Toast.makeText(getActivity(), "Saved Successfully", Toast.LENGTH_LONG).show();
                getBaseActivity().onBackPressed();
            }
        });
    }
}
