package com.servit.mobile.modules.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.servit.mobile.BaseActivity;
import com.servit.mobile.R;
import com.servit.mobile.framework.annotations.MenuActivity;
import com.servit.mobile.modules.auth.AuthActivity;
import com.servit.mobile.modules.rewards.RewardsActivity;
import com.servit.mobile.modules.sales.SalesActivity;
import com.servit.mobile.modules.shop.ShopActivity;

@MenuActivity(contentFragment = DashboardFragment.class)
public class DashboardActivity extends BaseActivity implements DashboardFragment.DashboardListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.home);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(this)
                .title(R.string.sign_out)
                .content(R.string.want_to_sign_out)
                .positiveText(R.string.yes)
                .negativeText(R.string.no)
                .theme(Theme.LIGHT)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        navigate(R.id.nav_logout);
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    }
                })
                .show();
    }

    @Override
    public void navigateToLogin() {
        Intent intent = new Intent(this, AuthActivity.class);
        startActivity(intent);
    }

    @Override
    public void navigateToSales() {
        Intent intent = new Intent(this, SalesActivity.class);
        startActivity(intent);
    }

    @Override
    public void navigateToRewards() {
        Intent intent = new Intent(this, RewardsActivity.class);
        startActivity(intent);
    }

    @Override
    public void navigateToNewSale() {
        Intent intent = new Intent(this, ShopActivity.class);
        startActivity(intent);
    }
}
