package com.servit.mobile.modules.rewards;

import android.os.Bundle;

import com.servit.mobile.BaseActivity;
import com.servit.mobile.R;
import com.servit.mobile.framework.annotations.MenuActivity;

@MenuActivity(contentFragment = RewardsFragment.class)
public class RewardsActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(getString(R.string.rewards));
    }
}
