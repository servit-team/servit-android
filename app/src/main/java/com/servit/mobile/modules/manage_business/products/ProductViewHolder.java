package com.servit.mobile.modules.manage_business.products;

import android.view.ViewGroup;
import android.widget.TextView;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.servit.mobile.R;
import com.servit.mobile.data.entities.product_service;

/**
 * Created by Brandon on 6/19/2016.
 */

public class ProductViewHolder extends BaseViewHolder<product_service> {

    TextView txtName;
    TextView txtQuantity;
    TextView txtCostPrice;
    TextView txtSalePrice;

    public ProductViewHolder(ViewGroup parent) {
        super(parent, R.layout.recycler_row_product);

        txtName = $(R.id.name);
        txtQuantity = $(R.id.quantity);
        txtCostPrice = $(R.id.cost_price);
        txtSalePrice = $(R.id.sale_price);
    }

    @Override
    public void setData(final product_service person){
        txtName.setText(getContext().getString(R.string.name_header) +  person.getName());
        txtQuantity.setText(getContext().getString(R.string.quantity_header) +  person.getQuantity());
        txtCostPrice.setText(getContext().getString(R.string.cost_price_header) +  person.getCp());
        txtSalePrice.setText(getContext().getString(R.string.sale_price_header) +  person.getSp());
    }
}
