package com.servit.mobile.modules.manage_business.products;

import android.widget.EditText;
import android.widget.Toast;

import com.servit.mobile.R;
import com.servit.mobile.data.BusinessDataStore;
import com.servit.mobile.data.IServiceHandler;
import com.servit.mobile.data.ServiceResult;
import com.servit.mobile.data.entities.product_service;
import com.servit.mobile.BaseFragment;
import com.servit.mobile.framework.annotations.FragmentContentView;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by Brandon on 6/15/2016.
 */
@FragmentContentView(R.layout.fragment_products_add)
public class ProductsAddFragment extends BaseFragment {

    BusinessDataStore businessDataStore;

    @Bind(R.id.product_name)
    EditText txtProductName;

    @Bind(R.id.product_description)
    EditText txtDescription;

    @Bind(R.id.quantity)
    EditText txtQuantity;

    @Bind(R.id.cost_price)
    EditText txtCostPrice;

    @Bind(R.id.sale_price)
    EditText txtSalesPrice;

    public static ProductsAddFragment getNewInstance() {
        return new ProductsAddFragment();
    }

    @Override
    protected void injectDependencies() {
        super.injectDependencies();

        businessDataStore = resolve(BusinessDataStore.class);
    }

    @OnClick(R.id.save)
    void save(){
        product_service newProduct = new product_service();

        newProduct.setName(txtProductName.getText().toString());
        newProduct.setDescription(txtDescription.getText().toString());
        newProduct.setQuantity(Integer.parseInt(txtQuantity.getText().toString()));
        newProduct.setCp(txtCostPrice.getText().toString());
        newProduct.setSp(txtSalesPrice.getText().toString());

        businessDataStore.addProduct(newProduct, new IServiceHandler<Void>() {
            @Override
            public void onComplete(ServiceResult<Void> result) {
                if (!result.getHasError()) {
                    Toast.makeText(getActivity(), "Success", Toast.LENGTH_LONG).show();
                    getActivity().onBackPressed();
                }
            }
        });
    }
}
