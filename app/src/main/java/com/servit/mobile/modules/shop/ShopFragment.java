package com.servit.mobile.modules.shop;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.processbutton.FlatButton;
import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.servit.mobile.BaseFragment;
import com.servit.mobile.R;
import com.servit.mobile.data.BusinessDataStore;
import com.servit.mobile.data.IServiceHandler;
import com.servit.mobile.data.ServiceResult;
import com.servit.mobile.data.entities.product_service;
import com.servit.mobile.framework.annotations.FragmentContentView;
import com.servit.mobile.framework.annotations.InstanceState;
import com.servit.mobile.modules.cart.ShoppingCart;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

import static android.support.design.R.attr.alpha;

/**
 * Created by Brandon on 6/10/2016.
 */

@FragmentContentView(R.layout.fragment_shop)
public class ShopFragment extends BaseFragment {

    @Bind(R.id.recycler)
    EasyRecyclerView recyclerView;

    BusinessDataStore businessDataStore;

    ShopClickListener shopClickListener;

    @InstanceState
    List<product_service> products;

    ProductAdapter adapter;
    private Menu menu;
    int count;

    @OnClick(R.id.cart)
    void cart() {
        shopClickListener.onShop();
    }

    public static ShopFragment getNewInstance() {
        return new ShopFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof ShopClickListener) {
            shopClickListener = (ShopClickListener) context;
        }
    }

    @Override
    protected void injectDependencies() {
        super.injectDependencies();

        businessDataStore = resolve(BusinessDataStore.class);

    }

    @Override
    public void onDetach() {
        super.onDetach();

        shopClickListener = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    private void bindRecycler(List<product_service> products) {
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        adapter = new ProductAdapter(getActivity());
        adapter.addAll(products);

        recyclerView.setAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        setTitle(getString(R.string.shop));

        if (products == null) {
            businessDataStore.loadProducts(new IServiceHandler<List<product_service>>() {
                @Override
                public void onComplete(ServiceResult<List<product_service>> result) {
                    if (!result.getHasError()) {
                        products = result.getResult();
                        bindRecycler(products);
                    } else {
                        Toast.makeText(getActivity(), "error loading products", Toast.LENGTH_LONG).show();
                    }
                }
            });
        } else {
            bindRecycler(products);
        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        inflater.inflate(R.menu.menu_cart, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cart:
                shopClickListener.onShop();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        menu.findItem(R.id.cart).setTitle(String.valueOf(count));
        Drawable drawable = menu.findItem(R.id.carticon).getIcon();
        if (drawable != null) {
            drawable.mutate();
            drawable.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            drawable.setAlpha(alpha);
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null) {
            bindRecycler(products);
        }
    }

    public class ProductViewHolder extends BaseViewHolder<product_service> {

        TextView txtName;
        TextView txtPrice;
        FlatButton btnMinus;
        FlatButton btnPlus;

        public ProductViewHolder(ViewGroup parent) {
            super(parent, R.layout.recycler_grid_product);

            txtName = $(R.id.name);
            txtPrice = $(R.id.price);
            btnMinus = $(R.id.btnMinus);
            btnPlus = $(R.id.btnPlus);
        }

        @Override
        public void setData(final product_service product) {
            txtName.setText(product.getName());
            txtPrice.setText(product.getCp());
            btnPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ++count;
                    ShoppingCart.getInstance().addItem(product);
                    getActivity().invalidateOptionsMenu();
                    if (ShoppingCart.getInstance().getCountForItem(product) >= 1) {
                        btnMinus.setVisibility(View.VISIBLE);
                    }
                }
            });

            btnMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    --count;
                    ShoppingCart.getInstance().removeItem(product);
                    getActivity().invalidateOptionsMenu();
                    if (!ShoppingCart.getInstance().getProducts().containsKey(product)) {
                        btnMinus.setVisibility(View.GONE);
                    }
                }
            });
        }
    }


    public class ProductAdapter extends RecyclerArrayAdapter<product_service> {
        public ProductAdapter(Context context) {
            super(context);
        }

        @Override
        public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            return new ShopFragment.ProductViewHolder(parent);
        }
    }

    public interface ShopClickListener {
        void onShop();
    }
}
