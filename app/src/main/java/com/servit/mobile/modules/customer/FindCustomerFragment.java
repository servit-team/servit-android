package com.servit.mobile.modules.customer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.servit.mobile.BaseFragment;
import com.servit.mobile.R;
import com.servit.mobile.data.CustomerDataStore;
import com.servit.mobile.data.IServiceHandler;
import com.servit.mobile.data.ServiceResult;
import com.servit.mobile.data.entities.business;
import com.servit.mobile.data.entities.customer;
import com.servit.mobile.data.entities.product_service;
import com.servit.mobile.framework.annotations.FragmentContentView;
import com.servit.mobile.framework.annotations.InstanceState;

import java.util.List;

import butterknife.Bind;

/**
 * Created by Brandon on 6/28/2016.
 */
@FragmentContentView(R.layout.fragment_find_customer)
public class FindCustomerFragment extends BaseFragment {

    @Bind(R.id.recycler)
    EasyRecyclerView recyclerView;

    CustomerAdapter customerAdapter;

    CustomerDataStore customerDataStore;

    @InstanceState
    business business;

    public static FindCustomerFragment getNewInstance(business business){
        FindCustomerFragment findCustomerFragment = new FindCustomerFragment();
        findCustomerFragment.business = business;

        return findCustomerFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!getIsRestored())
            customerDataStore.getCustomersForBusiness(business, new IServiceHandler<List<customer>>() {
                @Override
                public void onComplete(ServiceResult<List<customer>> result) {
                    if (!result.getHasError()) {
                        customerAdapter = new CustomerAdapter(getActivity());
                        customerAdapter.addAll(result.getResult());

                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recyclerView.setAdapter(customerAdapter);
                    }
                    else{
                        //TODO: error handler
                    }
                }
            });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        return view;
    }

    @Override
    protected void injectDependencies() {
        super.injectDependencies();

        customerDataStore = resolve(CustomerDataStore.class);
    }

    public class CustomerViewHolder extends BaseViewHolder<product_service> {

        TextView txtTitle;
        TextView txtSubtitle;

        public CustomerViewHolder(ViewGroup parent) {
            super(parent,R.layout.recycler_grid_product);

            txtTitle = $(R.id.title);
            txtSubtitle = $(R.id.subtitle);
        }

        @Override
        public void setData(final product_service product){
        }
    }

    public class CustomerAdapter extends RecyclerArrayAdapter<customer> {
        public CustomerAdapter(Context context) {
            super(context);
        }

        @Override
        public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            return new FindCustomerFragment.CustomerViewHolder(parent);
        }
    }

    public interface ShopClickListener{
        void onShop();
    }
}
