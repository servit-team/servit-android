package com.servit.mobile.modules.customer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.BackendlessUser;
import com.servit.mobile.BaseFragment;
import com.servit.mobile.R;
import com.servit.mobile.data.CustomerDataStore;
import com.servit.mobile.data.IServiceHandler;
import com.servit.mobile.data.ServiceResult;
import com.servit.mobile.data.UserDataStore;
import com.servit.mobile.data.entities.ServitUser;
import com.servit.mobile.data.entities.business;
import com.servit.mobile.framework.annotations.FragmentContentView;
import com.servit.mobile.framework.annotations.InstanceState;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Brandon on 6/28/2016.
 */
@FragmentContentView(R.layout.fragment_add_customer)
public class AddCustomerFragment extends BaseFragment {
    @Bind(R.id.phone)
    TextView txtPhone;
    @Bind(R.id.first_name)
    EditText txtFirstName;
    @Bind(R.id.last_name)
    EditText txtLastName;
    @Bind(R.id.email)
    EditText txtEmail;

    CustomerDataStore customerDataStore;
    UserDataStore userDataStore;

    @InstanceState
    business business;


    public static AddCustomerFragment getNewInstance(business business) {
        AddCustomerFragment addCustomerFragment = new AddCustomerFragment();
        addCustomerFragment.business = business;

        return addCustomerFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        txtPhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void injectDependencies() {
        super.injectDependencies();

        customerDataStore = resolve(CustomerDataStore.class);
        userDataStore = resolve(UserDataStore.class);
    }

    @OnClick(R.id.done)
    void onDoneClick() {
        //TODO: Should this be moved to the node server?

        final String email = txtEmail.getText().toString();
        final String phone = txtPhone.getText().toString();
        final String firstname = txtFirstName.getText().toString();
        final String lastname = txtLastName.getText().toString();

        userDataStore.getUserByEmail(email, new IServiceHandler<ServitUser>() {
            @Override
            public void onComplete(ServiceResult<ServitUser> result) {
            BackendlessUser user = result.getResult();

            if (user == null) {
                ServitUser servitUser = new ServitUser();

                servitUser.setEmail(email);
                servitUser.setPhone(phone);
                servitUser.setFirstname(firstname);
                servitUser.setLastname(lastname);

                userDataStore.createNewCustomerUser(servitUser, new IServiceHandler<ServitUser>() {
                    @Override
                    public void onComplete(ServiceResult<ServitUser> result) {
                        if (!result.getHasError()) {
                            userDataStore.sendCustomerEmail("You've been signed up!", "You have been signed up! Click <a href=\"www.google.com\">here</a>", email, new IServiceHandler<Void>() {
                                @Override
                                public void onComplete(ServiceResult<Void> result) {

                                }
                            });
                            Toast.makeText(getActivity(), "Success!", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.sorry_error_saving, Toast.LENGTH_LONG).show();
                        }
                    }
                });
            } else {
                Toast.makeText(getActivity(), "User already exists", Toast.LENGTH_LONG).show();
            }
            }
        });
    }
}
