package com.servit.mobile.modules.manage_business.employees;

import android.os.Bundle;

import com.servit.mobile.BaseActivity;
import com.servit.mobile.framework.annotations.MenuActivity;

@MenuActivity(contentFragment = ManageEmployeesFragment.class)
public class ManageEmployeesActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
