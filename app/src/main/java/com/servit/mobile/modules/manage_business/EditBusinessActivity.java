package com.servit.mobile.modules.manage_business;

import android.os.Bundle;

import com.servit.mobile.BaseActivity;
import com.servit.mobile.data.entities.business;
import com.servit.mobile.modules.manage_business.employees.ManageEmployeesFragment;
import com.servit.mobile.modules.manage_business.products.ProductsAddFragment;
import com.servit.mobile.modules.manage_business.products.ProductsFragment;
import com.servit.mobile.framework.annotations.MenuActivity;

@MenuActivity(contentFragment = ManageBusinessFragment.class)
public class EditBusinessActivity extends BaseActivity implements ManageBusinessFragment.ManageClickListener, ProductsFragment.AddProductListener {

    static final String EMPLOYEE_TAG = "employees";
    static final String PRODUCTS_TAG = "products";
    static final String PRODUCTS_ADD_TAG = "products_add";
    static final String BUSINESS_EDIT_TAG = "business_edit";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onEmployee() {
        replaceFragment(ManageEmployeesFragment.getNewInstance(), true, EMPLOYEE_TAG);
    }

    @Override
    public void onProducts() {
        replaceFragment(ProductsFragment.getNewInstance(), true, EMPLOYEE_TAG);
    }

    @Override
    public void onEdit(business business) {
        replaceFragment(EditBusinessFragment.getNewInstance(business), true, BUSINESS_EDIT_TAG);
    }

    @Override
    public void onAddProduct() {
        replaceFragment(ProductsAddFragment.getNewInstance(), true, PRODUCTS_ADD_TAG);
    }
}
