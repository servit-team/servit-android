package com.servit.mobile.modules.dashboard;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.servit.mobile.R;
import com.servit.mobile.data.entities.Reward;
import com.servit.mobile.framework.annotations.FragmentContentView;
import com.servit.mobile.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by Brandon on 6/10/2016.
 */

@FragmentContentView(R.layout.fragment_dashboard_rewards)
public class DashboardRewardsFragment extends BaseFragment {

    @Bind(R.id.recyclerRewards)
    EasyRecyclerView recyclerViewRewards;

    RewardAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        adapter = new RewardAdapter(getActivity());
        adapter.addAll(getMockData());
        adapter.notifyDataSetChanged();

        recyclerViewRewards.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewRewards.setAdapter(adapter);

        return view;
    }

    private List<Reward> getMockData(){
        List<Reward> rewards = new ArrayList<>();

        rewards.add(new Reward("Test reward 1"));
        rewards.add(new Reward("Test reward 2"));
        rewards.add(new Reward("Test reward 3"));
        rewards.add(new Reward("Test reward 4"));
        rewards.add(new Reward("Test reward 5"));
        rewards.add(new Reward("Test reward 6"));


        return rewards;
    }

    public class RewardAdapter extends RecyclerArrayAdapter<Reward> {
        public RewardAdapter(Context context) {
            super(context);
        }

        @Override
        public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            return new RewardViewHolder(parent);
        }
    }

    public class RewardViewHolder extends BaseViewHolder<Reward> {
        TextView txtName;

        public RewardViewHolder(ViewGroup parent) {
            super(parent,R.layout.row_dashboard_reward);
            txtName = $(R.id.txtName);
        }

        @Override
        public void setData(final Reward reward){
            txtName.setText(reward.getName());
        }
    }
}
