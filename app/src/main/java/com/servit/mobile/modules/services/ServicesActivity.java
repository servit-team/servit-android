package com.servit.mobile.modules.services;

import android.os.Bundle;

import com.servit.mobile.BaseActivity;
import com.servit.mobile.framework.annotations.MenuActivity;

@MenuActivity(contentFragment = ServiceFragment.class)
public class ServicesActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
