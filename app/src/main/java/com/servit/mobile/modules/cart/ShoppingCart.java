package com.servit.mobile.modules.cart;

import com.servit.mobile.data.entities.product_service;

import java.util.HashMap;

/**
 * Created by Brandon on 6/19/2016.
 */

public class ShoppingCart {
    private static ShoppingCart shoppingCart = new ShoppingCart();

    HashMap<product_service, Integer> products = new HashMap<>();

    public static ShoppingCart getInstance(){
        return shoppingCart;
    }

    public void addItem(product_service product){
        if (products.containsKey(product)){
            products.put(product, products.get(product) + 1);
        }
        else{
            products.put(product, 1);
        }
    }

    public void removeItem(product_service product){
        if (products.containsKey(product)){
            products.put(product, products.get(product) - 1);
            if (products.get(product) == 0){
                products.remove(product);
            }
        }
    }

    public HashMap<product_service, Integer> getProducts(){
        return products;
    }

    public int getCountForItem(product_service product) {
        return products.get(product);
    }
}
