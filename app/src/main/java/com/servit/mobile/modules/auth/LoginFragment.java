package com.servit.mobile.modules.auth;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.internal.MDButton;
import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.HeadersManager;
import com.backendless.exceptions.BackendlessException;
import com.dd.processbutton.iml.ActionProcessButton;
import com.servit.mobile.BaseFragment;
import com.servit.mobile.R;
import com.servit.mobile.data.UserDataStore;
import com.servit.mobile.data.entities.ServitUser;
import com.servit.mobile.data.entities.owner;
import com.servit.mobile.framework.UserSessionManager;
import com.servit.mobile.framework.annotations.FragmentContentView;

import butterknife.Bind;

import static com.servit.mobile.R.id.email;

/**
 * Created by Brandon on 6/10/2016.
 */

@FragmentContentView(R.layout.fragment_signin)
public class LoginFragment extends BaseFragment {

    // UI references.
    @Bind(R.id.btnLogin)
    ActionProcessButton btnLogin;

    @Bind(R.id.btnSignup)
    MDButton btnSignup;

    @Bind(email)
    EditText mEmailView;

    @Bind(R.id.password)
    EditText mPasswordView;

    @Bind(R.id.login_progress)
    View mProgressView;

    private UserLoginTask mAuthTask = null;

    LoginListener listener;
    Handler handler;
    UserDataStore userDataStore;

    public static LoginFragment getNewInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);


        //won't match parent to activity if we don't do this
        ViewGroup.LayoutParams contentViewLayout = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(contentViewLayout);

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        setTitle(getString(R.string.login));
        handler = new Handler();
        return view;
    }

    @Override
    protected void injectDependencies() {
        super.injectDependencies();

        userDataStore = resolve(UserDataStore.class);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnLogin.setMode(ActionProcessButton.Mode.ENDLESS);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToSignup();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        btnLogin.setProgress(0);
    }

    @Override
    public void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            btnLogin.setProgress(1);
            Backendless.Data.mapTableToClass("User", ServitUser.class);
            Backendless.Data.mapTableToClass("owner", owner.class);
//            userDataStore.login(email, password, new IServiceHandler<Boolean>() {
//                @Override
//                public void onComplete(ServiceResult<Boolean> result) {
//                    showProgress(false);
//
//                    if (result.getResult()) {
//                        btnLogin.setProgress(100);
//                        navigateToHome();
//                    } else {
//                        mPasswordView.setError(getString(R.string.error_incorrect_password));
//                        mPasswordView.requestFocus();
////                        mAuthTask = null;
//                        showProgress(false);
//                        btnLogin.setProgress(-1);
//                        handler.postDelayed(runnable, 1000);
//                    }
//                }
//            });
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= 3;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Backendless.Data.mapTableToClass("User", ServitUser.class);
            Backendless.Data.mapTableToClass("owner", owner.class);

            try {
                BackendlessUser user = Backendless.UserService.login(mEmail, mPassword);
                ServitUser servitUser = ServitUser.fromBackendlessUser(user);
                UserSessionManager.getInstance().setCurrentUser(HeadersManager.getInstance().getHeader(HeadersManager.HeadersEnum.USER_TOKEN_KEY), servitUser);

                return true;
            } catch (BackendlessException ex) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                btnLogin.setProgress(100);
                navigateToHome();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
                btnLogin.setProgress(-1);
                handler.postDelayed(runnable, 1000);
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            btnLogin.setProgress(0);

        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (LoginListener) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    void navigateToSignup() {
        listener.navigateToSignup();
    }

    void navigateToHome() {
        listener.navigateToHome();
    }

    interface LoginListener {
        void navigateToSignup();

        void navigateToHome();
    }

}
