package com.servit.mobile.modules.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.appinvite.AppInvite;
import com.google.android.gms.appinvite.AppInviteInvitationResult;
import com.google.android.gms.appinvite.AppInviteReferral;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.servit.mobile.BaseActivity;
import com.servit.mobile.R;
import com.servit.mobile.framework.annotations.ActivityView;
import com.servit.mobile.modules.dashboard.DashboardActivity;

@ActivityView(value = R.layout.activity_login)
public class AuthActivity extends BaseActivity implements LoginFragment.LoginListener, SignupFragment.SignupListener, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "AUTH_ACTIVITY";

    private static final String LOGIN_FRAGMENT = "LoginFragment";
    private static final String SIGNUP_FRAGMENT = "SignupFragment";
    GoogleApiClient mGoogleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        navigateToLogin();
        // Build GoogleApiClient with AppInvite API for receiving deep links
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(AppInvite.API)
                .build();

        // Check if this app was launched from a deep link. Setting autoLaunchDeepLink to true
        // would automatically launch the deep link if one is found.
        boolean autoLaunchDeepLink = false;
        AppInvite.AppInviteApi.getInvitation(mGoogleApiClient, this, autoLaunchDeepLink)
                .setResultCallback(
                        new ResultCallback<AppInviteInvitationResult>() {
                            @Override
                            public void onResult(@NonNull AppInviteInvitationResult result) {
                                if (result.getStatus().isSuccess()) {
                                    // Extract deep link from Intent
                                    Intent intent = result.getInvitationIntent();
                                    String deepLink = AppInviteReferral.getDeepLink(intent);

                                    Toast.makeText(AuthActivity.this, deepLink, Toast.LENGTH_SHORT).show();

                                } else {
                                    Log.d(TAG, "getInvitation: no deep link found.");
                                }
                            }
                        });
    }

    @Override
    public void navigateToSignup() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction tx = fragmentManager.beginTransaction();
        tx.replace(R.id.frag_content, SignupFragment.getNewInstance()).addToBackStack(SIGNUP_FRAGMENT).commit();
    }

    @Override
    public void navigateToHome() {
        onBackPressed();
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
    }

    public void navigateToLogin() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction tx = fragmentManager.beginTransaction();
        tx.replace(R.id.frag_content, LoginFragment.getNewInstance()).commit();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(AuthActivity.this, "FAIL BLOG", Toast.LENGTH_LONG);
    }
}
