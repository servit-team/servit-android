package com.servit.mobile.modules.services;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.servit.mobile.R;
import com.servit.mobile.framework.annotations.FragmentContentView;
import com.servit.mobile.BaseFragment;

/**
 * Created by Brandon on 6/10/2016.
 */

@FragmentContentView(R.layout.fragment_services)
public class ServiceFragment extends BaseFragment {

    public static ServiceFragment getNewInstance(){
        return new ServiceFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        setTitle("Services");

        return view;
    }
}
